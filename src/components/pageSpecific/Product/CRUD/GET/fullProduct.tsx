import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import ProductMainInfo from "../../ProductMainInfo";
import ProductGHGData from '../../ProductGHGData';
import ProductAlternatives from "../../ProductAlternatives";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import goTo from "../../../../../helper/goTo";

const FullProduct = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/product/get/full/' + id.name, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    function handleGoWindow(link: string) {
        window.open(link, "_blank")
    }

    const cards = fData && !fError ? 
        <>
            <div className="topBarFlex">
                <div>
                    <ProductMainInfo
                        productId={fData.data.product.id}
                        productName={fData.data.product.name ? fData.data.product.name : "no name set for that Id"}
                        productCode={fData.data.product.id ? fData.data.product.id : "No Id found"}
                        seller={fData.data.seller[0] !== undefined ? fData.data.seller[0].name : "Not provided"}
                        sellerId={fData.data.seller[0] !== undefined ? fData.data.seller[0].id : "Not provided"}
                        totalGHG={fData.data.totalEmission.length > 0 ? fData.data.totalEmission[0].ghgPerKG : "Not provided"}
                        greenPremium={20}
                        category={fData.data.catgeory.name ? fData.data.catgeory.name : "Not provided"}
                        description={fData.data.product.description}
                        imgUrl={fData.data.product.imgLink ? fData.data.product.imgLink : "https://www.fleischtheke.info/proxy/img.php?url=https://m.media-amazon.com/images/I/51s1Hdw4oNL._SL350_.jpg"}
                        eventBuy={() => handleGoWindow(fData.data.product.linkToBuy)}
                        eventEditProduct={() => goTo('/edit/product/' + fData.data.product.id)}
                        eventSource={() => goTo('/source/' + fData.data.source[0].id + '/' + fData.data.product.id)}
                        eventEditSource={() => goTo('/verify/source/' + fData.data.source[0].id + '/' + fData.data.product.id + '/product/0/new/1')}
                        needsSourceEdit={true}
                        status={fData.data.product.approvalStatus}
                        sellerSourceHostoryEvent={fData.data.seller[0] ? () => goTo('/source/' + fData.data.seller[0].sourceId + '/' + fData.data.product.id) : console.log('no')}
                        sellerSourceApprove={fData.data.seller[0] ? () => goTo('/verify/source/' + fData.data.seller[0].sourceId + '/' + fData.data.product.id + '/seller/0/new/' + fData.data.seller[0].id) : console.log("test")}
                        sellerSourceStatus={fData.data.seller[0] ? fData.data.seller[0].approvalStatus : 'not deifined'}
                        eventEditSeller={fData.data.seller.length !== 0 ? () => goTo('/edit/seller/' + fData.data.product.id) : () => goTo('/add/seller/to/product/' + fData.data.product.id)}
                        eventAddSeller={fData.data.seller.length === 0 ? () => goTo('/add/seller/to/product/' + fData.data.product.id) : () => goTo('/edit/seller/' + fData.data.product.id)}
                        sellerSourceId={fData.data.seller[0] ? fData.data.seller[0].sourceId : undefined}
                        productSourceId={fData.data.source[0] !== undefined ? fData.data.source[0].id : undefined}
                    ></ProductMainInfo>
                </div>
            </div>
            <ProductGHGData
                productName={fData.data.product.name}
                totalGHG={fData.data.totalEmission.length > 0 ? fData.data.totalEmission[0].ghgPerKG : "Not provided"}
                totalGHGSourceId={fData.data.totalEmission.length > 0 ? fData.data.totalEmission[0].sourceId : undefined}
                totalGHGEdit={() => goTo('/verify/source/' + fData.data.totalEmission[0].sourceId + '/' + fData.data.product.id + '/total/0/new/' + fData.data.totalEmission[0].id)}
                totalGHGStatus={fData.data.totalEmission[0]}
                greenPremium={20}
                GHGSingle={fData.data.singleEmission}
                EmissionReason={fData.data.emissionReason}
                eventEditProduct={() => goTo('/edit/product/' + fData.data.totalEmission[0].id + '/' + fData.data.product.id)}
                eventEditTotal={() => goTo('/edit/total/' + fData.data.totalEmission[0].id + '/' + fData.data.product.id)}
                // todo: use actual history link
                eventHistorySourceSingle={fData.data.singleEmission[id.index] ? () => goTo('/source/' + fData.data.singleEmission[id.index].sourceId + '/' + fData.data.product.id) : console.log("test")}
                eventHistorySoueceReason={fData.data.emissionReason[id.index] ? () => goTo('/source/' + fData.data.emissionReason[id.index].sourceId + '/' + fData.data.product.id) : console.log("test")}
                data={fData}
            ></ProductGHGData>

            <ProductAlternatives
                productId={fData.data.product.id}
                name={fData.data.product.name}
                alternativeProducts={fData.data.altternativeProduct}
                rawMaterials={fData.data.productMadeOfRawMaterial}
            ></ProductAlternatives>
        </>
        : <p>...Loading</p>

    return (
        <> { cards && fData ? <div>{cards}</div>
        : fError ? 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <h1 className="errorMessageProductPage">Please enter a valid id to receive data from Greenformed!</h1>
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default FullProduct;
