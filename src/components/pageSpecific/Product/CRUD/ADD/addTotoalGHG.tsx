import React, {useState} from 'react';
import Button from "../../../../BuildingBlocks/ButtonTemplate";
import { AuthContext } from "../../../../../App";

import { NewProductProps } from "../../newProduct";

const ProductCard = (props: NewProductProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [totalGHG, setTotalGHG] = useState<number>(0);
    const [sourceTitle, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [isSet, setIsSet] = useState<boolean>(false);

    let skipHandle: any = () => {
        props.func();
    }

    const postAddNewTotoalGHG = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/ghg/total/new/${authState.id}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitle,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    totalGHGEmissions: totalGHG,
                })
            }).then(res => {
                  return res.json();
              })
              .then((res) => {
                setSourceTitle("")
                setSourceLink("")
                setSourceDescription("")
                props.func();
                setIsSet(true);
              })
              .catch(error => {
                setIsSet(false);
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <>
        {
            isSet === false ?
            <>
            <h2 className="addProductCategoryHeadline">Add the Total maount og Green House Gases emitted through this product: </h2>
            <div className="addProductCategoryAddDiv">
                <h2 className="addProductCategoryHeadline">Add new GHG Data: </h2>
                <form onSubmit={postAddNewTotoalGHG}>
                    <label>Total GHG *</label> 
                    <input className="form-control me-2"
                        id="inputProductInfoSourceLink" 
                        type="number" 
                        pattern='[0-9]{0,5}'
                        aria-label="inputDescriptionProduct"
                        value={totalGHG}
                        onChange={(e: any) => setTotalGHG(+e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Title *</label> 
                    <input className="form-control mbe-2"
                        id="inputNewSellerSourceLink" 
                        type="text" 
                        placeholder="Source Title"
                        aria-label="inputDescriptionProduct"
                        value={sourceTitle}
                        onChange={(e) => setSourceTitle(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Link *</label> 
                    <input className="form-control mbe-2"
                        id="inputNewSellerSourceLink" 
                        type="text" 
                        placeholder="Source Link"
                        aria-label="inputDescriptionProduct"
                        value={sourceLink}
                        onChange={(e) => setSourceLink(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Description *</label> 
                    <textarea className="form-control me-2"
                        id="inputNewSellerSourceDescription" 
                        placeholder="Source Description"
                        aria-label="inputDescriptionProduct"
                        value={sourceDescription}
                        onChange={(e) => setSourceDescription(e.target.value)}
                        ></textarea>
                    <input type="submit" value="Submit" />
                </form>
            </div>
            { props.skipHandle ?
                <Button
                    event={skipHandle}
                    bg={"#41475E"}
                    border={'transparaent'}
                    width={'250px'}
                    color={'white'}
                    margin={'10px 0 0 0'}
                >
                    Skip this field
                </Button> : <></>
            }
            </>
            : <p>The total Emission of Green House Gases is set to {totalGHG} per KG produced.</p>
            }
        </>
    )
};

export default ProductCard;