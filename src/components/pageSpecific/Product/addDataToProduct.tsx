import {useState, useEffect} from 'react';
import { useParams } from "react-router-dom";
import MainBar from "../../../components/BuildingBlocks/MainBarTemplate";
import SkeletonTemplate from "../../../components/BuildingBlocks/Skeleton";
import CategorySelection from "./CRUD/ADD/addCategory";
import SellerSelection from './CRUD/ADD/addSeller';
import TotalGHG from "./CRUD/ADD/addTotoalGHG";
import SingleGHG from "./CRUD/ADD/addSingleGHG";
import Reason from "./CRUD/ADD/addReason";
import Alternative from "./CRUD/ADD/addAlternative";
import RawMaterial from "./CRUD/ADD/addRawMaterial";
import goTo from '../../../helper/goTo';

const AddToExsitingProduct = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/product/get/core/' + id.productId, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    let typeAdd: string = '';
    if (id.type === 'category') {typeAdd = 'a category'}
    else if (id.type === 'seller') {typeAdd = 'a seller'}
    else if (id.type === 'total') {typeAdd = 'the total Emission of GHG in KG'}
    else if (id.type === 'single') {typeAdd = 'a single GHG Emission in KG'}
    else if (id.type === 'reason') {typeAdd = 'a reason of GHG Emission'}
    else if (id.type === 'alternative') {typeAdd = 'an alternative product'}
    else if (id.type === 'rawmaterial') {typeAdd = 'a raw material'}

    return (

        <>
        { !fError ?
            <>
            <MainBar
                marginTop={'0'}
                marginBottom={'20px'}
            >
                <div className="topBarFlex">
                    <div>
                        { fData ? <h1 className="topBarHeadline">Add {typeAdd} to {fData.data.product.name}.</h1> : <></> }
                        <h3 className="topBarSubheadline">services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</h3>
                    </div>
                </div>
            </MainBar>

            {
                id.type === 'category' ?
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <CategorySelection
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></CategorySelection>
                </MainBar>
                : <></>
            }

            {
                id.type === 'seller' ?
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <SellerSelection
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></SellerSelection>
                </MainBar>
                : <></>
            }

            {
                id.type === 'total' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <TotalGHG
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></TotalGHG>
                </MainBar>
                : <></>
            }

            {
                id.type === 'single' ?
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <SingleGHG
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></SingleGHG>
                </MainBar>
                : <></>
            }

            {
                id.type === 'reason' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <Reason
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></Reason>
                </MainBar> 
                : <></>
            } 

            {
                id.type === 'alternative' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <Alternative
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></Alternative>
                </MainBar> 
                : <></>
            }

            {
                id.type === 'rawmaterial' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <RawMaterial
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></RawMaterial>
                </MainBar> 
                : <></>
            }
            </>
        : <div className="skeletonWidth">
            <MainBar marginTop={'20px'} marginBottom={'0'}>
                <h1>Please Provide valid data!</h1>
            </MainBar>
            <MainBar marginTop={'20px'} marginBottom={'0'}>
                <SkeletonTemplate numberOfLines={6} /> 
            </MainBar>
            <MainBar marginTop={'20px'} marginBottom={'0'}>
                <SkeletonTemplate numberOfLines={6} /> 
            </MainBar>
            </div>
        }
        </>
    )
};

export default AddToExsitingProduct;
