import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import ChnageHistoryComponent from "../components/pageSpecific/History/Change/controller";

class ChnageHistory extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Change History of data in the Greenformed database.</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

               <ChnageHistoryComponent></ChnageHistoryComponent>

            </Layout>
        )
    }
}

export default ChnageHistory;