type SourcePointProps = {
    typeChange: string,
    title: string, 
    status: string, 
    date: number, 
    link: string, 
    description: string, 
    sourceId: string,
    productId: string,
    originalValue: any,
    newValue: any,
    field: string,
    userId: string,
};

const UpdatedSource = (props: SourcePointProps) => {
    let date: any = new Date(props.date);
    let dateDate = date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getFullYear();
    let dateTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return (
        <div>
            <div className="dotSourceHis"></div> 
            <div>
                <p>The {props.typeChange} was updated at the following field: {props.field}</p>
                <p>Original Value: {props.originalValue}</p>
                <p>New Value: {props.newValue}</p>
                <p>Updated Source Title: {props.title}</p>
                <div>
                    <p>Source Link: </p>
                    <a href={props.link} target="_blank" rel="noreferrer">Click here</a>
                </div>
                <p>Source description: {props.description}</p>
                <a href={`/source/${props.sourceId}/${props.productId}`}>Go to source</a>
                <p>Date of source creation: {dateDate} {dateTime}</p>
                <p>Approval status: {props.status}</p>
                {/* TODO: make link right */}
                <a href={`/user/${props.userId}`}>Visit User Profile</a>
            </div>  
        </div>
    )
};

export default UpdatedSource;
