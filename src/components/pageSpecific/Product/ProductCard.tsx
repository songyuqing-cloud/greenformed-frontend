import MainBar from "../../BuildingBlocks/MainBarTemplate";
import Tag from "../../BuildingBlocks/TagTemplate";
import Button from "../../BuildingBlocks/ButtonTemplate";

import arrow from "../../../assests/Arrow.svg";
import pie from "../../../assests/pie-chart.png";
import devide from "../../../assests/alternate.png";
import placeholder from "../../../assests/newImg.png";

type ProductCardProps = {
    productId: string,
    productName: string, 
    totalGHG: number, 
    ghgSingle1: string,
    ghgSingle2: string,
    ghgSingle3: string,
    ghgSingle4: string,
    ghgReason1: string,
    ghgReason2: string,
    ghgReason3: string,
    category: string, 
    description: string, 
    similarProducts: number, 
    getMoreEvent: any,
    editInfoEvent: any,
    imgUrl: string,
    originallyAddedBy: string,  
};

const ProductCard = (props: ProductCardProps) => {
    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="productDiv">
                <div className="productMainInfoDiv">
                    <h3 className="productName">{props.productName}</h3>
                    <div className="productImgInfoDIv">
                        {/* <img alt={"Greenhouse Gas emission of " + props.productName} src={props.imgUrl} className="productImgCard" /> */}
                        <img alt={"Greenhouse Gas emission of " + props.productName} src={placeholder} className="productImgCard" />
                        <div className="productRightToImgDiv">
                            {
                                props.totalGHG < 0 ? <Tag bg={"green"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>GHG Negative Product</Tag>
                                : props.totalGHG === 0 ? <Tag bg={"yellow"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>GHG Neutral Product</Tag>
                                : <Tag bg={"#F3C3C3"} width={'120px'} height={'20px'} color={"#571529"} fontSize={"10px"}>GHG High Product</Tag>
                            }
                            <p className="infoP">Category: {props.category}</p>
                        </div>
                    </div>
                    <Button 
                        event={props.getMoreEvent}
                        bg={"#A3A9C3"}
                        border={'transparent'}
                        width={"250px"}
                        color={"#36354A"}
                        margin={"10px 0 0 0"}
                    >
                        More Information
                    </Button>
                </div>
                <div className="deviderProductInfo"></div>
                <div className="productGhgMainDiv">
                    <div className="productGhgDataTop">
                        <div className='totalGhgDiv'>
                            <h2 className="productGHGDAtaHeadlineTop">1 KG of {props.productName} produces {props.totalGHG} of GHGs.</h2>
                            <div className="arrowProductDiv">
                                <img src={arrow} alt={"Emission data of " + props.productName} className='arrowProductEmissionImg' />
                                <p>Reasons of Emissions: {props.ghgReason1}, {props.ghgReason2}, {props.ghgReason3}</p>
                            </div>
                        </div>
                    </div>
                    <div className="productGhgDataMiddle">
                        <h3 className="productGHGDAtaHeadlineTop">{props.ghgSingle1} is emitted the most</h3>
                        <div className="arrowProductDiv">
                            <img src={pie} alt={props.productName + "emits" + props.totalGHG + "KG of Green House Data"} className="imgProductInfoMiddleBottom" />
                            <p>other GHGs: {props.ghgSingle2}, {props.ghgSingle3}, {props.ghgReason2}</p>
                        </div>
                    </div>
                    <div className="productGhgDataBottom">
                        <div className="productAlternativeProductsDiv">
                            <h3 className="productGHGDAtaHeadlineTop">Alternative Products</h3>
                            <div className="arrowProductDiv">
                                <img src={devide} alt={"Green House Gas Free Product Alternative for products that emit Green House Gases."} className="imgProductInfoMiddleBottom" />
                                <p>There are {props.similarProducts} GHG free alternative products for {props.productName}</p>
                            </div>
                        </div>
                    </div>
                    <h4 className="productDescription">{props.description}</h4>
                </div>
                <div className="divEditAndAddedByProductCard">
                    <div className="ProductEditDiv">
                            <Button 
                                event={props.editInfoEvent}
                                bg={"#A3A9C3"}
                                border={'transparent'}
                                width={"250px"}
                                color={"#36354A"}
                                margin={"10px 0 0 0"}
                            >
                                Edit Information
                            </Button>   
                    </div>
                    <p className="addedByProductcard">Originally added by {props.originallyAddedBy}</p> 
                </div>
            </div>
        </MainBar>
    )
};

export default ProductCard;
