type SourcePointProps = {
    title: string,
    status: string,
    date: number,
    link: string,
    description: string,
    addedBy: string,
    userName: string,
    userId: string,
    userType: string,
};

const AddedSourcePoint = (props: SourcePointProps) => {
    let date: any = new Date(props.date);
    let dateDate = date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getFullYear();
    let dateTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return (
        <div>
            <div className="dotSourceHis"></div> 
            <h3>The reason of GHG emission first source:</h3>
            <div>
                <p>Source Title: {props.title}</p>
                <div>
                    <p>Source Link: </p>
                    <a href={props.link} target="_blank" rel="noreferrer">Click here</a>
                </div>
                <p>Source description: {props.description}</p>
                <p>Date of source creation: {dateDate} {dateTime}</p>
                <p>Approval status: {props.status}</p>
                <p>Added by: {props.userName}</p>
                <p>Role Type at creation date: {props.userType}</p>
                {/* TODO: make link right */}
                <a href={`/user/${props.userId}`}>Visit User Profile</a>
            </div>  
        </div>
    )
};

export default AddedSourcePoint;
