import React, { useState, useEffect } from 'react';
import SkeletonTemplate from "../../../BuildingBlocks/Skeleton";
import MainCard from '../../../BuildingBlocks/MainBarTemplate';
import { AuthContext } from "../../../../App";
import VerificationCard from "../../../pageSpecific/VerificationData/cardVerificationData";

const SourceApprovalReasonGHG = () => {
    const { state: authState } = React.useContext(AuthContext);
    const [typeRel, setTypeRel] = useState<string>('product');
    const [typeSource, setTypeSource] = useState<string>('new');
    const [pData, setPData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let role: string;
    if (authState.role === 'superuser') {role = 'superuser'} else {role = 'user'}

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, [typeSource, typeRel]);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/source/get/random/'+ typeRel + '/superuser/' + typeSource, { 
            // fetch('http://localhost:8000/api/o/source/get/random/'+ typeSource + '/' + role, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setPData(resData))
            .catch(err => setFError(err));
    }

    const cards = pData && pData.data.map((item: any) => 
        <VerificationCard
            key={item.source.id}
            typeRel={typeRel}
            sourceFor = {item.relationNode.dataValues.name ? item.relationNode.dataValues.name 
                : item.relationNode.dataValues.ghgPerKG ? item.relationNode.dataValues.ghgPerKG + " KG per 1KG sold"
                : item.relationNode.dataValues.reasonTitle ? item.relationNode.dataValues.reasonTitle
                : item.relationNode.dataValues.ghgName ? item.relationNode.dataValues.ghgName
                : item.relationNode.dataValues.name}
            sourceTitle = {item.source.title}
            sourceLink = {item.source.link}
            date = {item.source.createdAt}
            status = {item.source.status}
            descriptionSource = {item.source.description}
            sourceId = {item.source.id}
            productId = {item.source.productId}
            typeSource = {typeSource}
            index = {0}
            modelId = {item.source.relId}
        ></VerificationCard>
    )

    return (
        <> { cards && pData ? <div>
                <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApproveSourcesFetch'>Approve sources of Green House Gas Data</h1>
                </MainCard>
                {/* { role !== 'user' ? */}
                { role === 'user' ?
                <MainCard marginTop={'0'} marginBottom={'0'}>
                    <div onClick={() => setTypeSource('new')}>Added Data</div>
                    <div onClick={() => setTypeSource('update')}>Data requested to update</div>
                    <div onClick={() => setTypeSource('delete')}>Data requested to delete</div>
                    { typeSource === 'new' ?
                    <>
                        <h2 className='headlineApproveSourcesFetch'>Select the type of data you want to approve:</h2>
                        <div>
                            <div onClick={() => setTypeSource('product')}>Product</div>
                            <div onClick={() => setTypeSource('seller')}>Seller</div>
                            <div onClick={() => setTypeSource('total')}>Total Emission Data</div>
                            <div onClick={() => setTypeSource('single')}>Single Emission Data</div>
                            <div onClick={() => setTypeSource('reason')}>Reason of Emission Data</div>
                        </div> 
                    </>
                    : typeSource === 'update' ?
                    <>
                        <h2 className='headlineApproveSourcesFetch'>Select the type of update data you want to approve:</h2>
                        <div>
                            <div onClick={() => setTypeRel('product')}>Product</div>
                            <div onClick={() => setTypeRel('seller')}>Seller</div>
                            <div onClick={() => setTypeRel('single')}>Single Emission Data</div>
                            <div onClick={() => setTypeRel('reason')}>Reason of Emission Data</div>
                        </div> 
                    </> 
                    : typeSource === 'delete' ?
                    <>
                        <h2 className='headlineApproveSourcesFetch'>Select the type of deletion data you want to approve:</h2>
                        <div>
                            <div onClick={() => setTypeRel('product')}>Product</div>
                            <div onClick={() => setTypeRel('seller')}>Seller</div>
                            <div onClick={() => setTypeRel('total')}>Total Emission Data</div>
                            <div onClick={() => setTypeRel('single')}>Single Emission Data</div>
                            <div onClick={() => setTypeRel('reason')}>Reason of Emission Data</div>
                        </div> 
                    </> 
                    :<></>
                    }
                </MainCard> : <></>
            }
                
                {cards}
            </div>
        : fError ? 
            <p>Something went wrong</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalReasonGHG;