import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import UpdateFormMain from "./updateMainData";

const UpdateProduct = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/product/get/full/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    const cards = fData && !fError ? 
        <>
                <UpdateFormMain
                    name = {fData.data.product.name}
                    description= {fData.data.product.description}
                    linkToBuy= {fData.data.product.linkToBuy}
                    costPerProduct= {fData.data.product.costPerProduct}
                    imgLink= {fData.data.product.imgLink}
                    productId = {id.id}
                ></UpdateFormMain>
        </>
        : <p>...Loading</p>

    return (
        <> { cards ? <div>{cards}</div>
        : fError ? 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <h1 className="errorMessageProductPage">Please enter a valid id to receive data from Greenformed!</h1>
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default UpdateProduct;
