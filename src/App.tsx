import React, { Suspense } from 'react';
import './App.scss';
import { Route, Switch, withRouter, Router, Redirect } from 'react-router-dom';
import history from './helper/history';
import reducer from './store/reducer/auth';

import Home from "./containers/home";
import ProductPage from "./containers/product";
import UpdateProduct from "./containers/updateProduct";
import UpdateSeller from "./containers/updateSeller";
import UpdateSingle from "./containers/updateSingle";
import UpdateReason from "./containers/updateReason";
import DeleteTotal from "./containers/deleteTotalGHG";
import Source from "./containers/Source";
import SourceApproval from './containers/SourceApproval';
import NewProductPage from "./containers/newProduct";
import AddToExistingProduct from "./containers/addToExistingProduct";
import Login from "./containers/Login";
import SignUp from "./containers/SignUp";
import Profile from "./containers/userProfile";
import GetApprovalData from "./containers/approvalStart";
import ChangeHistory from "./containers/changeHistory";

const initialState = {
  isAuthenticated: false,
  token: null,
  id: null
};

export const AuthContext = React.createContext<any>(initialState);

function App() {

  const [state, dispatch] = React.useReducer(reducer, initialState);

  let routes = (
    <Switch>
      <Route path="/" exact component={Home}/>
      <Route path="/product/:name" exact component={ProductPage}/>
      <Route path="/source/:id/:type/:name" exact component={Source}/>
      <Route path="/user/login" exact component={Login}/>
      <Route path="/user/signup" exact component={SignUp}/>
      <Route path="/change/history/:type/:id" exact component={ChangeHistory}/>
      <Suspense fallback={<div>Loading...</div>}>
        <Redirect to="/user/login"/>
      </Suspense>
    </Switch>
  );

  if (state.isAuthenticated) {
    routes = (
      <Switch>
        <Route path="/" exact component={Home}/>
        <Suspense fallback={<div>Loading...</div>}>
          <Route path="/product/:name" exact component={ProductPage}/>
          <Route path="/edit/product/:id" exact component={UpdateProduct}/>
          <Route path="/edit/seller/:id" exact component={UpdateSeller}/>
          <Route path="/edit/ghg/single/:id" exact component={UpdateSingle}/>
          <Route path="/edit/ghg/reason/:id" exact component={UpdateReason}/>
          <Route path="/edit/ghg/total/:id" exact component={DeleteTotal}/>
          <Route path="/source/:id/:type/:name" exact component={Source}/>
          <Route path="/new/product" exact component={NewProductPage}/>
          <Route path="/add/:type/to/product/:productId" exact component={AddToExistingProduct}/>
          <Route path="/user/profile" exact component={Profile}/>
          <Route path="/verify/source/:id/:product/:type/:index/:typeSource/:modelId" exact component={SourceApproval}/>
          <Route path="/approval/init/data" exact component={GetApprovalData}/>
          <Route path="/changeHistory/:type/:id" exact component={ChangeHistory}/>
        </Suspense>
      </Switch>
    );
  }

  return (
    <div className="App">
    <AuthContext.Provider
        value={{
          state,
          dispatch
        }}
      >
          <Router history={history}>
              <Suspense fallback={<p>Loading...</p>}>{routes}</Suspense>
          </Router>
        </AuthContext.Provider>
    </div>
  );
}

export default withRouter(App);