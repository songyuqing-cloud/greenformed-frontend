import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import MainCard from "../../../../BuildingBlocks/MainBarTemplate";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import { AuthContext } from "../../../../../App";
import UpdateForm from "../../../../BuildingBlocks/UpdateForm";
import DeleteForm from "../../../../BuildingBlocks/requestDeleteForm";
import goTo from "../../../../../helper/goTo";

const UpdateReasonData = () => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [response, setResponse] = useState<string>('');
    const [sourceTitel, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDesc, setSourceDesc] = useState<string>('');
    const [reasonGHGChanged, setReasonGHGChanged] = useState<string>('');
    const [reasonGHGDescriptionChanged, setReasonGHGDescriptionChanged] = useState<string>('');
    const [GHGNameChanged, setGHGNameChanged] = useState<string>('');
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();
    const [selectedField, setSelectedField] = useState<string>('');

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/product/get/full/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    const updateExistingData = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/ghg/reason/update/fields/${fData.data.emissionReason[0].id}/${authState.id}/${id.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token    
                },
                body: JSON.stringify({
                    reasonTitle: reasonGHGChanged,
                    description: reasonGHGDescriptionChanged,
                    ghgName: GHGNameChanged,
                    sourceTitle: sourceTitel,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(res => {
                console.log(res)
                return res.json();
              })
              .then(() => {
                setResponse("success")
                setReasonGHGChanged('')
                setReasonGHGDescriptionChanged('')
                setGHGNameChanged('')
                setSourceTitle('')
                setSourceLink('')
                setSourceDesc('')
                goTo('/product/' + id.id)
              })
              .catch(error => {
                  console.log("error: "+error)
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

      const deleteData = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/ghg/reason/request/deletion/${fData.data.emissionReason[0].id}/${authState.id}/${id.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token    
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitel,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(res => {
                console.log(res)
                return res.json();
              })
              .then(() => {
                setResponse("success")
                setSourceTitle('')
                setSourceLink('')
                setSourceDesc('')
                goTo('/product/' + id.id)
              })
              .catch(error => {
                  console.log("error: "+error)
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <MainCard
            marginTop={"0"}
            marginBottom={'20px'}
        >
        { fData ?
            <div className="containerFormUpdateProduct">
                    <div>
                        <h2>Reason of {fData.data.emissionReason[0].ghgName} Emission: {fData.data.emissionReason[0].reasonTitle}</h2>
                        <p>Choose the field that you want to update: </p>

                        <div className='updateSelctButtonsDiv'>
                            <div className='updateSelectDivSelect' id='title' onClick={() => setSelectedField('title')}>GHG Emission Reason Title</div>
                            <div className='updateSelectDivSelect' id='description' onClick={() => setSelectedField('description')}>GHG Emission Reason Description</div>
                            <div className='updateSelectDivSelect' id='name' onClick={() => setSelectedField('name')}>GHG Name</div>
                            <div className='updateSelectDivSelect' id='delete' onClick={() => setSelectedField('delete')}>Request to delete this reason</div>
                        </div>

                        {
                            selectedField === 'title' ?
                            <form onSubmit={updateExistingData}>
                                <UpdateForm
                                    eventUpdateField={setReasonGHGChanged}
                                    label={'Update the title of the Reason:'}
                                    typeUpdate={'text'}
                                    value={reasonGHGChanged}
                                    originalValue={fData.data.emissionReason[0].reasonTitle}
                                    eventSourceTitle={setSourceTitle}
                                    sourceTitle={sourceTitel}
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                />
                                <input type="submit" value="Submit" />
                            </form>
                            : selectedField === 'description' ?
                            <form onSubmit={updateExistingData}>
                                <UpdateForm
                                    eventUpdateField={setReasonGHGDescriptionChanged}
                                    label={'Update the description of the emission reason:'}
                                    typeUpdate={'text'}
                                    value={reasonGHGDescriptionChanged}
                                    originalValue={fData.data.emissionReason[0].description}
                                    eventSourceTitle={setSourceTitle}
                                    sourceTitle={sourceTitel}
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                />
                                <input type="submit" value="Submit" />
                            </form>
                            : selectedField === 'name' ?
                            <form onSubmit={updateExistingData}>
                                <UpdateForm
                                    eventUpdateField={setGHGNameChanged}
                                    label={'Update the name of the GHG:'}
                                    typeUpdate={"text"}
                                    value={GHGNameChanged}
                                    originalValue={fData.data.emissionReason[0].ghgName}
                                    eventSourceTitle={setSourceTitle}
                                    sourceTitle={sourceTitel}
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                />
                                <input type="submit" value="Submit" />
                            </form>
                            : selectedField === 'delete' ?
                            <form onSubmit={deleteData}>
                                <DeleteForm
                                    eventSourceTitle={setSourceTitle}
                                    sourceTitle={sourceTitel}
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                    name={fData.data.emissionReason[0].reasonTitle + ' (listed as a reason of GHG Emission in Greenformed) '}
                                />
                                <input type="submit" value="Submit" />
                            </form>
                            : <></>
                        }


                    </div>
                    {response ? <p>Success</p> : <></>}
                </div> :
                    fError ? <div className="skeletonWidth">
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <h1 className="errorMessageProductPage">Please enter a valid id to receive data from Greenformed!</h1>
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                </div> :
                <div className="skeletonWidth">
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    </div> 
            }
        </MainCard>
    )
};

export default UpdateReasonData;
