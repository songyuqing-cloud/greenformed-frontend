import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from "../../../../BuildingBlocks/MainBarTemplate";
import SellerCard from "../Seller/SellerCard";
import AddedFirst from "../Seller/fristAdded";
import UpdatedSource from "../Seller/updateSource";
import DeletionSource from "../Seller/deleteSource";

const ProductCardsFetched = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

      let id: any = useParams();

    const loadData = async () => {
        fetch(`http://localhost:8000/api/o/sel/changehis/get/` + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }   

    const updatedSources = fData && fData.data.updateSource[0] !== undefined && fData.data.updateSource[0].length !== 0 && fData.data.updateSource.reverse().map((item: any) => 
        <UpdatedSource
            key={item.source.id}
            typeChange={"seller"}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            originalValue={item.properties.OriginalValue}
            newValue={item.properties.NewValue}
            field={item.properties.ChangedField}
            userId={item.properties.UserId}
        />
    )

    const deleteionSource = fData && fData.data.requestDeletion[0] !== undefined && fData.data.requestDeletion[0].length !== 0  && fData.data.requestDeletion.reverse().map((item: any) => 
        <DeletionSource
            key={item.source.id}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            userId={item.properties.UserId}
            oriName={item.properties.OriginalName}
            oriLink={item.properties.OriginalLink}
        />
    )

    return (
        <> { fData && !fError ? 
                <>
                    <MainCard marginTop={'0'} marginBottom={'0'}>
                        <SellerCard
                            link={fData.data.seller.link}
                            name={fData.data.seller.name}
                        ></SellerCard>
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <AddedFirst
                           title={fData.data.source[0].title}
                           status={fData.data.source[0].status}
                           date={fData.data.source[0].createdAt}
                           link={fData.data.source[0].link}
                           description={fData.data.source[0].description}
                           userName={fData.data.addedBy[0].name}
                           userId={fData.data.addedBy[0].id}
                           userType={fData.data.addedBy[0].roleStatus}
                        ></AddedFirst>
                    </MainCard>
                    { fData && fData.data.updateSource[0] !== undefined ?
                        <MainCard marginTop={'20px'} marginBottom={'0'}>
                            {updatedSources}
                        </MainCard> :<></>
                    }
                    { fData && fData.data.requestDeletion[0] !== undefined ?
                        <MainCard marginTop={'20px'} marginBottom={'0'}>
                            {deleteionSource}
                        </MainCard> : <></>
                    }
                </>
            : fError ?
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    Please Provide correct data
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            :
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            } 
        </>
    )
};

export default ProductCardsFetched;
