import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import SourceDetails from "../../sourceDetails";
import ApprovalForm from "../approveSourceForm";
import { AuthContext } from "../../../../../App";

const SourceApprovalDeletedTotal = () => {
    const { state: authState } = React.useContext(AuthContext);
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/ghg/total/get/source/' + id.modelId + '/' + id.id, { 
            method: 'get', 
            headers: {
                "Content-Type": "application/json",
                'authorization': 'Bearer ' + authState.token
            },
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }

    const sources = fData && !fError ? 
        <>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApprovalSource'>Approve or deny the request deletion source of the total ghg Emission: {fData.data.total.ghgPerKG} KG (current value)</h1>
            </MainCard> 
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h4>The following data displays the total emission as it was as it was requested to be deleted:</h4>
                <h2>Per 1 KG sold {fData.data.deletionSource[0].properties.ghgPerKG} KG are emitted.</h2>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
            <h2>The data should be deleted due to the following: </h2>
                <SourceDetails
                    type={'total emission data requested to be deleted'}
                    name={fData.data.total.ghgPerKG + ' KG'}
                    titel={fData.data.deletionSource[0].source.title}
                    description={fData.data.deletionSource[0].source.description}
                    link={fData.data.deletionSource[0].source.link}
                    status={fData.data.deletionSource[0].source.status}
                ></SourceDetails>
            </MainCard>
            { fData.data.deletionSource[0].source.status !== 'approvedFinal' && fData.data.deletionSource[0].source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={fData.data.deletionSource[0].source.id}
                        productId={id.product}
                        typeSource='total'
                        modelId={fData.data.total.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && fData ? <div>{sources}</div>
        : fError ? 
            <p>Please provid a valid ID</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalDeletedTotal;
