import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../BuildingBlocks/Skeleton";
import MainCard from '../../../BuildingBlocks/MainBarTemplate';
import SourceHistory from '../sourceHistory';
import MainBar from "../../../BuildingBlocks/MainBarTemplate";
import SourceDetails from "../sourceDetails";

type ApprovalHistoryProps = {
    name: string,
    type: string,
};

const ApprovalHistory = (props: ApprovalHistoryProps) => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/source/get/id/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    const firstApprovalData: object | null = fData && fData.data.approvedFirstBy.user && !fError ? fData.data.approvedFirstBy : null;
    const finalApprovalData: object | null = fData && fData.data.approvedFinalBy.user && !fError ? fData.data.approvedFinalBy : null;
    const newUserApproval: object | null = fData && fData.data.approvedByNewUser.user && !fError ? fData.data.approvedByNewUser : null;
    const firstDenial: object | null = fData && fData.data.notApprovedOnceBy.user && !fError ? fData.data.notApprovedOnceBy : null;
    const finalDenial: object | null = fData && fData.data.notApprovedAgainBy.user && !fError ? fData.data.notApprovedAgainBy : null;
    const newUserDenial: object | null = fData && fData.data.notApprovedByNewUser.user && !fError ? fData.data.notApprovedByNewUser : null;

    const sources = fData && !fError ? 
        <>
            <MainBar
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
                <SourceDetails
                    key={props.name}
                    type={props.type}
                    name={props.name}
                    titel={fData.data.source.title}
                    description={fData.data.source.description}
                    link={fData.data.source.link}
                    status={fData.data.source.status}
                ></SourceDetails>
            </MainBar>
            <SourceHistory
                dataSource={props.type + ": "}
                dataName={props.name}
                createdAt={fData.data.source.createdAt}
                addedBy={fData.data.addedBy.addedBy}
                createdDescr={fData.data.source.description}
                firstApprovalData={firstApprovalData}
                finalApprovalData={finalApprovalData}
                newUserApprovalData={newUserApproval}
                firstDenialData={firstDenial}
                finalDenialData={finalDenial}
                newUserDenialData={newUserDenial}
            ></SourceHistory>
        </>
        : <p>...Loading</p>

    return (
        <> { sources && fData ? <div>{sources}</div>
        : fError ? 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <p>Please provid a valid ID</p>
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default ApprovalHistory;
