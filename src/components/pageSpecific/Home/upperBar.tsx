import React from 'react';
import MainBar from "../../BuildingBlocks/MainBarTemplate";
import Button from "../../BuildingBlocks/ButtonTemplate";
import goTo from "../../../helper/goTo";

const Navigation: React.FC = () => {

    return (
    <MainBar
        marginTop={'0'}
        marginBottom={'20px'}
    >
        <div className="topBarFlex">
            <div>
                <h1 className="topBarHeadline">Greenformed: The global Green House Gas (GHG) Database</h1>
                <h3 className="topBarSubheadline">services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</h3>
                <div className="upperBarDivButtons">
                    <div className='divUpperBarButton'>
                        <Button 
                            event={() => goTo('/approval/init/data')}
                            bg={"#A3A9C3"}
                            border={'#A3A9C3'}
                            width={"215px"}
                            color={"#36354A"}
                            margin={"20px 0 0 0"}
                        >
                            Verify Data
                        </Button>
                        <Button 
                            event={() => goTo('/new/product')}
                            bg={"#A3A9C3"}
                            border={'#A3A9C3'}
                            width={"215px"}
                            color={"#36354A"}
                            margin={"20px 0 0 0"}
                        >
                            Add Data
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    </MainBar>
    );
};

export default Navigation;
