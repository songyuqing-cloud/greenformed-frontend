type SingleCardProps = {
    ghgName: string, 
    emissionPerKG: number,
};

const SingleCard = (props: SingleCardProps) => {
    return (
            <div>
                <h1>The change history of the following single GHG Emission (current version): </h1>
                <p>GHG Name: {props.ghgName}</p>
                <p>Per 1 KG produced, {props.emissionPerKG} KG of {props.ghgName} are emitted.</p>
            </div>
    )
};

export default SingleCard;
