import React, {useState} from 'react';
import Button from "../../../../BuildingBlocks/ButtonTemplate";
import { AuthContext } from "../../../../../App";

import { NewProductProps } from "../../newProduct";

const ProductCard = (props: NewProductProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    let [timesSend, setTimesSend] = useState<number>(0);
    const [singleGHG, setSingleGHG] = useState<number>(0);
    const [ghgName, setGHGName] = useState<string>('')
    const [sourceTitle, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');

    let skipHandle: any = () => {
        props.func();
    }

    const postAddNewSingleGHG = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/ghg/single/new/${authState.id}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitle,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    ghgName: ghgName,
                    emissionPerKG: singleGHG,
                })
            }).then(res => {
                  return res.json();
              })
              .then((res) => {
                setSourceTitle("")
                setSourceLink("")
                setSourceDescription("")
                setGHGName('')
                setSingleGHG(0)
                setTimesSend(1);
                props.func();
              })
              .catch(error => {
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <>
            {
                timesSend === 0 ? <h2 className="addProductCategoryHeadline">Add a single Green House Gases Emission Data: </h2> 
                : <h2 className="addProductCategoryHeadline">Add another single Green House Gases Emission Data: </h2>
            }
            <div className="addProductCategoryAddDiv">
                <h2 className="addProductCategoryHeadline">Add new GHG Data: </h2>
                <form onSubmit={postAddNewSingleGHG}>
                    <label className="labelNewProduct">GHG Name *</label> 
                    <input className="form-control me-2"
                        id="inputNewSeller" 
                        type="text" 
                        placeholder="GHG Name"
                        aria-label="inputDescriptionProduct"
                        value={ghgName}
                        onChange={(e) => setGHGName(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Amout of GHG in KG per 1 KG of product *</label> 
                    <input className="form-control me-2"
                        id="inputNewSeller" 
                        type="number" 
                        placeholder="123"
                        aria-label="inputDescriptionProduct"
                        value={singleGHG}
                        onChange={(e: any) => setSingleGHG(+e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Title *</label> 
                    <input className="form-control me-2"
                        id="inputNewSellerSourceLink" 
                        type="text" 
                        placeholder="Source Link"
                        aria-label="inputDescriptionProduct"
                        value={sourceTitle}
                        onChange={(e) => setSourceTitle(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Link *</label> 
                    <input className="form-control me-2"
                        id="inputNewSellerSourceLink" 
                        type="text" 
                        placeholder="Source Link"
                        aria-label="inputDescriptionProduct"
                        value={sourceLink}
                        onChange={(e) => setSourceLink(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Description *</label> 
                    <textarea className="form-control me-2"
                        id="inputNewSellerSourceDescription" 
                        placeholder="Source Description"
                        aria-label="inputDescriptionProduct"
                        value={sourceDescription}
                        onChange={(e) => setSourceDescription(e.target.value)}
                        ></textarea>
                    <input type="submit" value="Submit" />
                </form>
            </div>
            { props.skipHandle ?
                <Button
                        event={skipHandle}
                        bg={"#41475E"}
                        border={'transparaent'}
                        width={'250px'}
                        color={'white'}
                        margin={'10px 0 0 0'}
                    >
                        Skip this field
                </Button> : <></>
            }
        </>
    )
};

export default ProductCard;