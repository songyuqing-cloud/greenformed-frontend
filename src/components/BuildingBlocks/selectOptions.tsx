type SelectOptionsProps = {
    event: any,
    text: string,
};

const SelectOptions = (props: SelectOptionsProps) => {
    return (
        <div className='selectOptiosnDivContainter' onClick={() => props.event()}>
            <p className='selectOptiosText'>{props.text}</p>
        </div>
    )
};

export default SelectOptions;