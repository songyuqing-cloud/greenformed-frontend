type ProductCardProps = {
    productId: string,
    productName: string, 
    description: string,
    linkToBuy: string,
    costPerProduct: number,
    sourceId: string,
};

const ProductCard = (props: ProductCardProps) => {
    return (
            <div>
                <h1>The change history of the following product (current version): </h1>
                <p>Product Name: {props.productName}</p>
                <p>Description: {props.description}</p>
                <p>Costs Per Unit: {props.costPerProduct}</p>
                <a href={props.linkToBuy} target="_blank" rel="noreferrer">Link to buy</a>
                <div>
                    <p>More information about {props.productName}: </p>
                    <a href={`/product/${props.productId}`}>click here</a>
                </div>
                <div>
                    <p>Original Source: </p>
                    <a href={`/source/${props.sourceId}/${props.productId}`}>click here</a>
                </div>
            </div>
    )
};

export default ProductCard;
