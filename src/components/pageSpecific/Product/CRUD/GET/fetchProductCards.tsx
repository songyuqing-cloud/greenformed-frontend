import { useState, useEffect } from 'react';
import ProductCard from "../../ProductCard";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from "../../../../BuildingBlocks/MainBarTemplate";
import goTo from "../../../../../helper/goTo";

const ProductCardsFetched = () => {
    const [fData, setFData] = useState<any>();

    useEffect(() => {
        loadData();
      }, []);

    const loadData = async () => {
        fetch(`http://localhost:8000/api/o/product/get/random`, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData));
    }   

    const cards = fData && fData.data.map((item: any) => 
        <ProductCard
            key={item.product.dataValues.id}
            productId={item.product.dataValues.id}
            productName={item.product.dataValues.name}
            totalGHG={item.productTotalEmission[0] ? item.productTotalEmission[0].ghgPerKG : 0}
            ghgSingle1={item.productSingleEmission[0] ? item.productSingleEmission[0].ghgName : "No data yet"}
            ghgSingle2={item.productSingleEmission[1] ? item.productSingleEmission[1].ghgName : "No data yet"}
            ghgSingle3={item.productSingleEmission[2] ? item.productSingleEmission[2].ghgName : "No data yet"}
            ghgSingle4={item.productSingleEmission[3] ? item.productSingleEmission[3].ghgName : "No data yet"}
            ghgReason1={item.productEmissionReason[0] ? item.productEmissionReason[0].ghgName : "No data yet"}
            ghgReason2={item.productEmissionReason[1] ? item.productEmissionReason[1].ghgName : "No data yet"}
            ghgReason3={item.productEmissionReason[2] ? item.productEmissionReason[2].ghgName : "No data yet"}
            category={item.productCategory.name ? item.productCategory.name : "Not provided"}
            description={item.product.dataValues.description}
            similarProducts={12}
            originallyAddedBy={'Peter Mafay'}
            getMoreEvent={() => goTo("/product/" + item.product.dataValues.id)}
            editInfoEvent={() => goTo("/edit/" + item.product.dataValues.id)}
            imgUrl={item.product.dataValues.imgLink ? item.product.dataValues.imgLink : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSBxJ_vcwu8YGx8f6mVaUe5BIjk3XyMfa-XKg&usqp=CAU"}
        ></ProductCard>)

    return (
        <> { cards && fData ? <div>{cards}</div> 
            : 
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            } 
        </>
    )
};

export default ProductCardsFetched;
