import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from "../../../../BuildingBlocks/MainBarTemplate";
import ProdutCard from "../Product/ProductCard";
import FirstSource from "../Product/firstSourcePointChange";
import UpdatedSource from "../Product/updateSources";
import DeletionSource from "../Product/deleteSource";

const ProductCardsFetched = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

      let id: any = useParams();

    const loadData = async () => {
        fetch(`http://localhost:8000/api/o/changehis/get/` + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }   

    const updatedSources = fData && fData.data.updateSource[0] !== undefined && fData.data.updateSource[0].length !== 0 && fData.data.updateSource.reverse().map((item: any) => 
        <UpdatedSource
            key={item.source.id}
            typeChange={"product"}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            sourceId={item.source.id}
            productId={fData.data.product.id}
            originalValue={item.properties.OriginalValue}
            newValue={item.properties.NewValue}
            field={item.properties.ChangedField}
            userId={item.properties.UserId}
        />
    )

    const deleteionSource = fData && fData.data.deletionRequestSource[0] !== undefined && fData.data.deletionRequestSource[0].length !== 0  && fData.data.deletionRequestSource.reverse().map((item: any) => 
        <DeletionSource
            key={item.source.id}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            sourceId={item.source.id}
            productId={fData.data.product.id}
            userId={item.properties.UserId}
            originalDescription={item.properties.OriginalDescription}
            OriginalLinkToBuy={item.properties.OriginalLinkToBuy}
            OriginalName={item.properties.OriginalName}
        />
    )

    return (
        <> { fData && !fError ? 
                <>
                    <MainCard marginTop={'0'} marginBottom={'0'}>
                        <ProdutCard
                            productId={fData.data.product.id}
                            productName={fData.data.product.name}
                            description={fData.data.product.description}
                            linkToBuy={fData.data.product.linkToBuy}
                            costPerProduct={fData.data.product.costPerProduct}
                            sourceId={fData.data.product.sourceId}
                        ></ProdutCard>
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <FirstSource
                            productId={fData.data.product.id}
                            title={fData.data.source[0].title}
                            status={fData.data.source[0].status}
                            date={fData.data.source[0].createdAt}
                            link={fData.data.source[0].link}
                            description={fData.data.source[0].description}
                            addedBy={fData.data.addedBy[0].description}
                            userName={fData.data.addedBy[0].name}
                            userId={fData.data.addedBy[0].id}
                            userType={fData.data.addedBy[0].description}
                            sourceId={fData.data.source[0].id}
                        ></FirstSource>
                    </MainCard>
                    { fData && fData.data.updateSource[0] !== undefined ?
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        {updatedSources}
                    </MainCard> :<></>
                    }
                    { fData && fData.data.deletionRequestSource[0] !== undefined ?
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        {deleteionSource}
                    </MainCard> : <></>
                    }
                </>
            : fError ?
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    Please Provide correct data
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            :
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            } 
        </>
    )
};

export default ProductCardsFetched;
