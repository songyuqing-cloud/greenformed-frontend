import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import MainBar from "../components/BuildingBlocks/MainBarTemplate";
import ProfileData from "../components/pageSpecific/Auth/User/profileData";

class UserProfile extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>
                <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                    >
                    <div className="topBarFlex">
                        <div>
                            <h1 className="topBarHeadline">This is your profile Page</h1>
                            <h3 className="topBarSubheadline">services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</h3>
                        </div>
                    </div>
                </MainBar>

                <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                >
                    <ProfileData
                        name={"Test"}
                        email={"trst@maiol.de"}
                        role={"new"}
                        createdAt={'Gestern'}
                        company={'123Test'}
                    ></ProfileData>
                </MainBar>

            </Layout>
        )
    }
}

export default UserProfile;