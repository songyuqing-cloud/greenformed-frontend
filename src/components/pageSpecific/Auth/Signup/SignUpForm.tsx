import {useState} from 'react';
import goTo from "../../../../helper/goTo";

const SignUpForm = () => {

    const initialState = {
        isSubmitting: false,
        errorMessage: null
    };

    const [data, setData] = useState(initialState);
    const [name, setName] = useState('');
    const [company, setCompany] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const postSignUp = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch("http://localhost:8000/api/signup/user", {
            method: "post",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              email: email,
              password: password,
              name: name, 
              company: company,
            })
            }).then(res => {
                if (res.ok) {
                    setEmail("");
                    setPassword("");
                    setCompany("");
                    setName("");
                    goTo('/user/login')
                    return res.json();
                }
                throw res;
            })
            .catch(error => {
                setData({
                ...data,
                isSubmitting: false,
                errorMessage: error.statusText
                });
            });
      }

    return (
        <>
        <form onSubmit={postSignUp}>
            <div className="newProductTopDiv">
                <div className="newProductMainInfosDiv">
                    <div className="newProductNameDiv">
                        <div className="newProductInputDiv">
                            <label>Name *</label> 
                            <input className="form-control me-2"
                                id="inputProductInfo" 
                                type="text" 
                                placeholder="Name"
                                aria-label="inputName"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                ></input>
                        </div>
                        <div className="newProductInputDiv">
                            <label>Company</label> 
                            <input className="form-control me-2"
                                id="inputProductInfo" 
                                type="text" 
                                placeholder="Company"
                                aria-label="inputCompany"
                                value={company}
                                onChange={(e) => setCompany(e.target.value)}
                                ></input>
                        </div>
                        <div className="newProductInputDiv">
                            <label>Email Address *</label> 
                            <input className="form-control me-2"
                                id="inputProductInfo" 
                                type="text" 
                                placeholder="Email Address"
                                aria-label="inputEmail"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                ></input>
                        </div>
                        <div className="newProductInputDiv">
                            <label>Password *</label> 
                            <input className="form-control me-2"
                                id="inputProductInfo" 
                                type="text" 
                                placeholder="Password"
                                aria-label="inputPassword"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                ></input>
                        </div>
                    </div>
                </div>
                <div className="buttonDivNewData">
                <input type="submit" value="Submit" />
                </div>
            </div>
            {data.errorMessage && (
                    <span className="form-error" title="Error message">{data.errorMessage}</span>
                )}
            </form>
        </>
    )
};

export default SignUpForm;