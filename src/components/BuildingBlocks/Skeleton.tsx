import Skeleton from 'react-loading-skeleton';

type SkeletonProps = {
    numberOfLines: number,
};

const SkeletonTemplate = (props: SkeletonProps) => {
    return (
        <div>
            <Skeleton height={50} />
            <div className="skeletonDevider"></div>
            <Skeleton count={props.numberOfLines} height={25}/>
        </div>
    )
};

export default SkeletonTemplate;