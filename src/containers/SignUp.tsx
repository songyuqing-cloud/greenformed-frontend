import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import MainBar from "../components/BuildingBlocks/MainBarTemplate";
import SignUpform from "../components/pageSpecific/Auth/Signup/SignUpForm";

class NewProduct extends Component {
    
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Sign Up and join to the Greenformed - the plattform for green house gas data</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                    <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                    >
                        <div className="topBarFlex">
                            <div>
                                <h1 className="topBarHeadline">Sign Up to Greenformed to get the full experience</h1>
                                <h3 className="topBarSubheadline">services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</h3>
                            </div>
                        </div>
                    </MainBar>

                    <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                    >
                        Sign Up Page
                        <SignUpform></SignUpform>
                    </MainBar>

            </Layout>
        )
    }
}

export default NewProduct;