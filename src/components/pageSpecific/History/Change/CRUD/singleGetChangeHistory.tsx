import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from "../../../../BuildingBlocks/MainBarTemplate";
import SingleCard from "../Single/singleCard";
import AddedFirst from "../Seller/fristAdded";
import UpdatedSource from "../Seller/updateSource";
import DeletionSource from "../Single/deletionSource";

const SingleChangeHistory = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

      let id: any = useParams();

    const loadData = async () => {
        fetch(`http://localhost:8000/api/o/ghg/single/changehis/get/` + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }   

    const updatedSources = fData && fData.data.updateSource[0] !== undefined && fData.data.updateSource[0].length !== 0 && fData.data.updateSource.reverse().map((item: any) => 
        <UpdatedSource
            key={item.source.id}
            typeChange={"single GHG emission"}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            originalValue={item.properties.OriginalValue}
            newValue={item.properties.NewValue}
            field={item.properties.ChangedField}
            userId={item.properties.UserId}
        />
    )

    const deleteionSource = fData && fData.data.deletionSource[0] !== undefined && fData.data.deletionSource[0].length !== 0  && fData.data.deletionSource.reverse().map((item: any) => 
        <DeletionSource
            key={item.source.id}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            userId={item.properties.UserId}
            oriGHGName={item.properties.ghgName}
            oriEmission={item.properties.emissionPerKG}
        />
    )

    return (
        <> { fData && !fError ? 
                <>
                    <MainCard marginTop={'0'} marginBottom={'0'}>
                        <SingleCard
                            ghgName={fData.data.singleEmission.ghgName}
                            emissionPerKG={fData.data.singleEmission.emissionPerKG}
                        ></SingleCard>
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <AddedFirst
                           title={fData.data.source[0].title}
                           status={fData.data.source[0].status}
                           date={fData.data.source[0].createdAt}
                           link={fData.data.source[0].link}
                           description={fData.data.source[0].description}
                           userName={fData.data.addedBy[0].name}
                           userId={fData.data.addedBy[0].id}
                           userType={fData.data.addedBy[0].roleStatus}
                        ></AddedFirst>
                    </MainCard>
                    { fData && fData.data.updateSource[0] !== undefined ?
                        <MainCard marginTop={'20px'} marginBottom={'0'}>
                            {updatedSources}
                        </MainCard> :<></>
                    }
                    { fData && fData.data.deletionSource[0] !== undefined ?
                        <MainCard marginTop={'20px'} marginBottom={'0'}>
                            {deleteionSource}
                        </MainCard> : <></>
                    }
                </>
            : fError ?
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    Please Provide correct data
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            :
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            } 
        </>
    )
};

export default SingleChangeHistory;
