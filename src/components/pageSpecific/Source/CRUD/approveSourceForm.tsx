import React, {useState} from 'react';
import { AuthContext } from "../../../../App";

type SourceApprovalProps = {
    sourceId: string, 
    productId: string,
    typeSource: string,
    modelId: string,
};

const VerifySource = (props: SourceApprovalProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [description, setDescription] = useState<string>('');
    const [approvalStatus, setApprovalStatus] = useState<boolean>(false);
    const [response, setResponse] = useState<string>(''); 

    const putSourceStatus = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });

          //TODO change back
    
        fetch(`http://localhost:8000/api/approval/source/${authState.id}/${props.modelId}/${props.sourceId}/${authState.role}/${props.typeSource}/${props.productId}/0`, {
            // fetch(`http://localhost:8000/api/approval/source/${authState.id}/${props.modelId}/${props.sourceId}/superuser/${props.typeSource}/${props.productId}/0`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                },
                body: JSON.stringify({
                    description: description,
                    approval: approvalStatus,
                })
            }).then(res => {
                console.log(res)
                if (res.ok) {
                    setDescription('');
                    setResponse("Successfully updated");
                    return res.json();
                }
                setResponse("Same user is not allowed to approve same or own source.");
                throw res;
              })
              .catch(error => {
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <>
            <div className="addProductCategoryAddDiv">
                <h2 className="addProductCategoryHeadline">Add new GHG Data: </h2>
                <form onSubmit={putSourceStatus}>
                    <label className="labelNewProduct">Do you verify the source?</label> 
                    <input type="checkbox"
                            id="verified" 
                            name="verified" 
                            value="verified"
                            onChange={() => setApprovalStatus(!approvalStatus)}
                            />
                    <label className="labelNewProduct">Description of the Descision *</label> 
                    <textarea className="form-control me-2"
                        id="inputNewSellerSourceDescription" 
                        placeholder="Description of Descision"
                        aria-label="inputDescriptionProduct"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        ></textarea>
                    <input type="submit" value="Submit" />
                </form>
                {response ? <p>{response}</p> : <></>}
            </div>
        </>
    )
};

export default VerifySource;