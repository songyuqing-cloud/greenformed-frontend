import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import SourceDetails from "../../sourceDetails";
import ApprovalForm from "../approveSourceForm";

const SourceApprovalProduct = () => {
    const [pData, setPData] = useState<any>();
    const [sData, setSData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadProdData();
        loadSourceData();
        // eslint-disable-next-line
      }, []);

    const loadProdData = async () => {
        fetch('http://localhost:8000/api/o/product/get/core/' + id.product, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setPData(resData))
            .catch(err => setFError(err));
    }

    const loadSourceData = async () => {
        fetch('http://localhost:8000/api/o/source/get/id/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setSData(resData))
            .catch(err => setFError(err));

    } 

    const sources = pData && sData ? 
        <>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApprovalSource'>Approve or deny the source of the product {pData.data.product.name}</h1>
            </MainCard> 
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h2>The provided data of the product: {pData.data.product.name}</h2>
                <h4>The description is: {pData.data.product.description}</h4>
                <p>You can buy the product at: <a href={pData.data.product.linkToBuy} rel="noreferrer" target='_blank'>{pData.data.product.linkToBuy}</a></p>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
                <SourceDetails
                    type={'product'}
                    name={pData.data.product.name}
                    titel={sData.data.source.title}
                    description={sData.data.source.description}
                    link={sData.data.source.link}
                    status={sData.data.source.status}
                ></SourceDetails>
            </MainCard>
            { sData.data.source.status !== 'approvedFinal' && sData.data.source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={sData.data.source.id}
                        productId={pData.data.product.id}
                        typeSource='product'
                        modelId={pData.data.product.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && pData ? <div>{sources}</div>
        : fError ? 
            <p>Please provid a valid ID</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalProduct;
