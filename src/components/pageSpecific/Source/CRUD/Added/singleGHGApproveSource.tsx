import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import SourceDetails from "../../sourceDetails";
import ApprovalForm from "../approveSourceForm";
import ApproveProduct from "./prodApproveSource";

const SourceApprovalSingleGHG = () => {
    const [pData, setPData] = useState<any>();
    const [sData, setSData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadTotalData();
        loadSourceData();
        // eslint-disable-next-line
      }, []);

    const loadTotalData = async () => {
        fetch('http://localhost:8000/api/o/product/get/full/' + id.product, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setPData(resData))
            .catch(err => setFError(err));
    }

    const loadSourceData = async () => {
        fetch('http://localhost:8000/api/o/source/get/id/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setSData(resData))
            .catch(err => setFError(err));
    } 

    const sources = pData && sData && !fError ? 
        <>
        { 
            pData.data.product.status !== 'approvedFinal' ?
                <>
                    <MainCard marginTop={"0"}
                        marginBottom={'20px'}>
                        <h2>Before updating the Emission reason, please approve the product first.</h2>
                    </MainCard>
                    <ApproveProduct></ApproveProduct>
                </>
            : <>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApprovalSource'>Approve or deny the source of {pData.data.singleEmission[id.index].ghgName} emissions for {pData.data.product.name}</h1>
            </MainCard> 
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                   <h2 className='headlineApprovalSourceGHG'>Total {pData.data.singleEmission[id.index].ghgName} emission of {pData.data.product.name} per 1kg: {pData.data.singleEmission[id.index].emissionPerKG} kg</h2>
            </MainCard> 
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
                <SourceDetails
                    type={'single GHG Emission'}
                    name={pData.data.product.name}
                    titel={sData.data.source.title}
                    description={sData.data.source.description}
                    link={sData.data.source.link}
                    status={sData.data.source.status}
                ></SourceDetails>
            </MainCard>
            { sData.data.source.status !== 'approvedFinal' && sData.data.source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                {/* TODO: index change  */}
                    <ApprovalForm
                        sourceId={pData.data.singleEmission[id.index].sourceId}
                        modelId={pData.data.singleEmission[id.index].id}
                        typeSource='single'
                        productId={pData.data.product.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
            </>
        }
        </>
        : <p>...Loading</p>

    return (
        <> { sources  && pData ? <div>{sources}</div>
        : fError ? 
            <p>Please provid a valid ID</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalSingleGHG;