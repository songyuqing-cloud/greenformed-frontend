import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import SourceDetails from "../../sourceDetails";
import ApprovalForm from "../approveSourceForm";

const SourceApprovalDeletedSeller = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/seller/get/source/' + id.modelId + '/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }

    const sources = fData && !fError ? 
        <>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApprovalSource'>Approve or deny the request deletion source of the seller {fData.data.seller.name}</h1>
            </MainCard> 
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h4>The following data displays the seller as it was as it was requested to be deleted:</h4>
                <h2>The provided data of the seller requested to be deleted: {fData.data.deletionSource[0].properties.OriginalName}</h2>
                <p>The seller's link is: <a href={fData.data.deletionSource[0].properties.OriginalLink} rel="noreferrer" target='_blank'>Click here</a></p>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
            <h2>The data should be deleted due to the following: </h2>
                <SourceDetails
                    type={'seller requested to be deleted'}
                    name={fData.data.seller.name}
                    titel={fData.data.deletionSource[0].source.title}
                    description={fData.data.deletionSource[0].source.description}
                    link={fData.data.deletionSource[0].source.link}
                    status={fData.data.deletionSource[0].source.status}
                ></SourceDetails>
            </MainCard>
            { fData.data.deletionSource[0].source.status !== 'approvedFinal' && fData.data.deletionSource[0].source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={fData.data.deletionSource[0].source.id}
                        productId={id.product}
                        typeSource='seller'
                        modelId={fData.data.seller.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && fData ? <div>{sources}</div>
        : fError ? 
            <p>Please provid a valid ID</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalDeletedSeller;
