import React, {useState } from 'react';
import Button from "../../../../BuildingBlocks/ButtonTemplate";
import Search from "../../../../BuildingBlocks/SearchBar";
import { AuthContext } from "../../../../../App";
import SelectOptions from "../../../../BuildingBlocks/selectOptions";

import { NewProductProps } from "../../newProduct";

const AddCategory = (props: NewProductProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [category, setCategory] = useState<any>();
    const [existingCategory, setExistingCategory] = useState<any>();
    const [isSet, setIsSet] = useState<boolean>(false);
    const [searchParam, setSearchParam] = useState<string>('');
    const [searchResult, setSearchResult] = useState<any>();

    let skipHandle: any = () => {
        props.func();
    }

    const searchForCategory = async () => {
        fetch(`http://localhost:8000/api/o/category/get/${searchParam}`, { 
            method: 'GET', 
            headers: {
                "Content-Type": "application/json",   
            },
          }).then(response => response.json())
            .then(resData => setSearchResult(resData))
    }    

    const postAddCategory = (e: any) => {
        console.log(props.productId)
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/category/new/${authState.id}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                },
                body: JSON.stringify({
                    typeOfCategory: category 
                })
            }).then(res => {
                  return res.json();
              })
              .then((res) => {
                console.log(res);
                props.func();
              }).then(() => {setIsSet(true);})
              .catch(error => {
                  console.log(error)
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

      const putExistingCategory = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/product/update/first/relationship/category/${props.productId}/${existingCategory}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                 },
            }).then(res => {
                  return res.json();
              })
              .then((res) => {
                console.log(res);
                setIsSet(true);
                props.func();
              })
              .catch(error => {
                  console.log(error)
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <>  {
            isSet === false ?
            <>
                <h2 className="addProductCategoryHeadline">Add a category to the product.</h2>
                    <label>The category is:</label>
                    <div className="categorySelectDivFlex">
                        <div>
                            <Search
                                event={searchForCategory}
                                bg='white'
                                fontColor='grey'
                                borderColor='grey'
                                border='grey'
                                icon=''
                                placeholder='search existing categories'
                                width='300px'
                                changeEvent={(e: any) => setSearchParam(e.target.value)}
                            >Search</Search>
                            {
                                searchResult && searchResult.data.category !== false ? 
                                <div>
                                    <SelectOptions
                                        event={() => setExistingCategory(searchResult.data.category.dataValues.id)}
                                        text={searchResult.data.category.dataValues.typeOfCategory}
                                    />
                                    { existingCategory ? 
                                        <Button
                                            event={putExistingCategory}
                                            bg={"#41475E"}
                                            border={'transparaent'}
                                            width={'250px'}
                                            color={'white'}
                                            margin={'10px 0 0 0'}
                                        >Set as category</Button> : <></>
                                    }
                                    
                                </div> :
                                searchResult && searchResult.data.category === false ?
                                <p>{searchParam} does not exist in our database. Feel free to add it below.</p> :
                                <></>
                            }
                        </div>
                        <div className="addProductCategoryAddDiv">
                            <form onSubmit={postAddCategory}>
                                <h2 className="addProductCategoryHeadline">Or add a new category: </h2>
                                <label>Category</label> 
                                <input className="form-control me-2"
                                    id="inputNewCategory" 
                                    type="text" 
                                    placeholder="Source Link"
                                    aria-label="inputDescriptionProduct"
                                    value={category}
                                    onChange={(e) => setCategory(e.target.value)}
                                    ></input>
                                <input type="submit" value="Submit" />
                            </form>
                        </div>
                    </div>
                    { props.skipHandle ?
                    <Button
                        event={skipHandle}
                        bg={"#41475E"}
                        border={'transparaent'}
                        width={'250px'}
                        color={'white'}
                        margin={'10px 0 0 0'}
                    >
                        Skip this field
                    </Button> : <></>
                    }
                </>
                : category && isSet ? 
                    <div>
                        <p>The category is set to {category}.</p>
                    </div>
                : <div>
                    <p>The category is set.</p>
                </div>
                }
        </>
    )
};

export default AddCategory;