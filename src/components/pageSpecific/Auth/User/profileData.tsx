import React, { useState, useEffect } from 'react';
import Edit from "../../../BuildingBlocks/simpleEdit";
import { AuthContext } from "../../../../App";
import SkeletonTemplate from "../../../BuildingBlocks/Skeleton";
import MainCard from "../../../BuildingBlocks/MainBarTemplate";

type ProfileProps = {
    name: string, 
    email: string, 
    role: string, 
    createdAt: string, 
    company: string,
};

const ProfileData = (props: ProfileProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const [fData, setFData] = useState<any>();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(`http://localhost:8000/api/user/data/${authState.id}`, { 
            method: 'get', 
            headers: {
                "Content-Type": "application/json",
                'authorization': 'Bearer ' + authState.token    
            },
          }).then(response => response.json())
            .then(resData => setFData(resData));
    }   

    return (
        <>
            {
                fData ?
                <div className="profileDataContainer">
                    <div className="profileCoreDiv">
                        <p>Name: {fData.data.name}</p>
                        <p>Email: {fData.data.email}</p>
                        <p>Prodile Type: {fData.data.role}</p>
                        <p>Profile exists since: {fData.data.createdAt}</p>
                        <p>Company: {fData.data.company}</p>
                    </div>
                    <Edit 
                        event={console.log("Profile")}
                        editText={"Edit your Profile"}
                    ></Edit>
                </div> 
                : 
                <>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                </>
            }
        </>
    )
};

export default ProfileData;
