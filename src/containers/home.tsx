import  { Component } from 'react';
import { Helmet } from 'react-helmet';
import Layout from "../components/HOC/Layout";
import MainBar from "../components/BuildingBlocks/MainBarTemplate";
import Button from "../components/BuildingBlocks/ButtonTemplate";
import ProductCards from "../components/pageSpecific/Product/CRUD/GET/fetchProductCards";
import UpperBar from "../components/pageSpecific/Home/upperBar";

class Index extends Component {
    render() {
        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                    <UpperBar></UpperBar>

                    <ProductCards></ProductCards>

                    <MainBar
                        marginTop={'0'}
                        marginBottom={'0'}
                    >
                        <div className="bottomBarFlex">
                            <h3 className="bottomBarHeadline">Do you want to get more data?</h3>
                            <Button 
                                event={console.log("Main")}
                                bg={"#A3A9C3"}
                                border={'#A3A9C3'}
                                width={"215px"}
                                color={"#36354A"}
                                margin={"20px 0 0 0"}
                            >
                                Load more data
                            </Button>
                        </div>
                    </MainBar>

            </Layout>
        )
    }
}

export default Index;