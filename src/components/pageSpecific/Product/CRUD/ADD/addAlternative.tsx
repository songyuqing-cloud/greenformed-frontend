import React, {useState } from 'react';
import Button from "../../../../BuildingBlocks/ButtonTemplate";
import Search from "../../../../BuildingBlocks/SearchBar";
import { AuthContext } from "../../../../../App";
import SelectOptions from "../../../../BuildingBlocks/selectOptions";

import { NewProductProps } from "../../newProduct";

const AddAlternative = (props: NewProductProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [existingCategory, setExistingCategory] = useState<any>();
    let [timesSend, setTimesSend] = useState<number>(0);
    const [searchParam, setSearchParam] = useState<string>('');
    const [searchResult, setSearchResult] = useState<any>();

    let skipHandle: any = () => {
        props.func();
    }

    const searchForCategory = async () => {
        fetch(`http://localhost:8000/api/o/search/name/${searchParam}/approved`, { 
            method: 'GET', 
            headers: {
                "Content-Type": "application/json",   
            },
          }).then(response => response.json())
            .then(resData => setSearchResult(resData))
    }    

    const putExistingCategory = (e: any) => {
    e.preventDefault();
    setData({
        ...data,
        isSubmitting: true,
        errorMessage: null
        });

    fetch(`http://localhost:8000/api/product/update/relationship/alternative/${props.productId}/${existingCategory}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                'authorization': 'Bearer ' + authState.token
                },
        }).then(res => {
                return res.json();
            })
            .then((res) => {
            setTimesSend(1);
            props.func();
            })
            .catch(error => {
                setData({
                    ...data,
                    isSubmitting: false,
                    errorMessage: error.statusText
                });
              }
            );
      }

    return (
            <>
                {
                    timesSend === 0 ? <h2 className="addProductCategoryHeadline">Add an alternative Product:</h2> 
                    : <h2 className="addProductCategoryHeadline">Add another alternative product: </h2>
                }
                    <label>The alternative is:</label>
                    <div className="categorySelectDivFlex">
                        <div>
                            <Search
                                event={searchForCategory}
                                bg='white'
                                fontColor='grey'
                                borderColor='grey'
                                border='grey'
                                icon=''
                                placeholder='search existing categories'
                                width='300px'
                                changeEvent={(e: any) => setSearchParam(e.target.value)}
                            >Search</Search>
                            {
                                searchResult && searchResult.data.responseObj[0] !== undefined ? 
                                <div>
                                    <SelectOptions
                                        event={() => setExistingCategory(searchResult.data.responseObj.dataValues.id)}
                                        text={searchResult.data.responseObj.dataValues.name}
                                    />
                                    { existingCategory ? 
                                        <Button
                                            event={putExistingCategory}
                                            bg={"#41475E"}
                                            border={'transparaent'}
                                            width={'250px'}
                                            color={'white'}
                                            margin={'10px 0 0 0'}
                                        >Set as alternative</Button> : <></>
                                    }
                                    
                                </div> :
                                searchResult && searchResult.data.responseObj[0] === undefined ?
                                <p>{searchParam} does not exist in our approved database.</p> :
                                <></>
                            }
                        </div>
                    </div>
                    { props.skipHandle ?
                        <Button
                            event={skipHandle}
                            bg={"#41475E"}
                            border={'transparaent'}
                            width={'250px'}
                            color={'white'}
                            margin={'10px 0 0 0'}
                        >
                            Skip this field
                        </Button> : <></>
                    }
                </> 
    )
};

export default AddAlternative;