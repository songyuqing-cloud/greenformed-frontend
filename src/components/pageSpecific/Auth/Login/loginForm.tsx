import React, {useState} from 'react'
import { useHistory } from 'react-router';

import { AuthContext } from "../../../../App";

const LoginForm = () => {

    const routerHistory = useHistory();

    const { dispatch } = React.useContext(AuthContext);

    const initialState = {
        isSubmitting: false,
        errorMessage: null
    };

    const [data, setData] = useState(initialState);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const postLogin = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch("http://localhost:8000/api/login/user", {
            method: "post",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              email: email,
              password: password
            })
            }).then(res => {
                if (res.ok) {
                    setEmail("");
                    setPassword("");
                    return res.json();
                }
                throw res;
            })
            .then(resJson => {
                dispatch({
                    type: "LOGIN",
                    payload: resJson
                })
                console.log("Loged in: " + resJson.data.token)
                routerHistory.goBack();
            })
            .catch(error => {
                setData({
                ...data,
                isSubmitting: false,
                errorMessage: error.statusText
                });
            });
      }

    return (
        <>
            <form onSubmit={postLogin}>
                <div className="newProductTopDiv">
                    <div className="newProductMainInfosDiv">
                        <div className="newProductNameDiv">
                            <div className="newProductInputDiv">
                                <label>Email Address *</label> 
                                <input className="form-control me-2"
                                    id="inputProductInfo" 
                                    type="text" 
                                    placeholder="Email Address"
                                    aria-label="inputEmail"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    ></input>
                            </div>
                            <div className="newProductInputDiv">
                                <label>Password *</label> 
                                <input className="form-control me-2"
                                    id="inputProductInfo" 
                                    type="text" 
                                    placeholder="Password"
                                    aria-label="inputPassword"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    ></input>
                            </div>
                        </div>
                    </div>
                    <div className="buttonDivNewData">
                        <input type="submit" value="Submit" />
                    </div>
                </div>
                {data.errorMessage && (
                        <span className="form-error" title="Error message">{data.errorMessage}</span>
                    )}
            </form>
        </>
    )
};

export default LoginForm;