type DeletionSourceProps = {
    title: string, 
    status: string, 
    date: number, 
    link: string, 
    description: string, 
    userId: string,
    oriGHGName: string,
    oriEmission: number,
};

const DeleteSource = (props: DeletionSourceProps) => {
    let date: any = new Date(props.date);
    let dateDate = date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getFullYear();
    let dateTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return (
        <div>
            <div className="dotSourceHis"></div> 
            <div>
                <p>Original Single GHG Name: {props.oriGHGName}</p> 
                <p>Original Emission per 1KG sold: {props.oriEmission} KG of {props.oriGHGName}</p>
                <div>
                    <p>Source Link: </p>
                    <a href={props.link} target="_blank" rel="noreferrer">Click here</a>
                </div>
                <p>Source description: {props.description}</p>
                <p>Date of source creation: {dateDate} {dateTime}</p>
                <p>Approval status: {props.status}</p>
                {/* TODO: make link right */}
                <a href={`/user/${props.userId}`}>Visit User Profile</a>
            </div>  
        </div>
    )
};

export default DeleteSource;
