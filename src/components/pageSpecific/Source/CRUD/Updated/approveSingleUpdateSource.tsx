import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import SourceDetails from "../../sourceDetails";
import ApprovalForm from "../approveSourceForm";
import { AuthContext } from "../../../../../App";

const SourceApprovalUpdatedSingle = () => {
    const { state: authState } = React.useContext(AuthContext);
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/ghg/single/get/source/' + id.modelId + '/' + id.id, { 
            method: 'get', 
            headers: {
                "Content-Type": "application/json",
                'authorization': 'Bearer ' + authState.token
            },
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }

    const sources = fData ? 
        <>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApprovalSource'>Approve or deny the update source of the single GHG emission {fData.data.single.ghgName}</h1>
            </MainCard> 
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h2>The name of the updated ghg emission: {fData.data.single.ghgName}</h2>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
            <h2>The data was updated as following:</h2>
            <p>Changed Field: Emission Per KG</p>
            <p>Original Value: {fData.data.updateSource[0].properties.OriginalValue} KG</p>
            <p>New Value: {fData.data.updateSource[0].properties.NewValue} KG</p>
                <SourceDetails
                    type={'updated single GHG emission'}
                    name={fData.data.single.ghgName}
                    titel={fData.data.updateSource[0].source.title}
                    description={fData.data.updateSource[0].source.description}
                    link={fData.data.updateSource[0].source.link}
                    status={fData.data.updateSource[0].source.status}
                ></SourceDetails>
            </MainCard>
            { fData.data.updateSource[0].source.status !== 'approvedFinal' && fData.data.updateSource[0].source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={fData.data.updateSource[0].source.id}
                        productId={id.product}
                        typeSource='single'
                        modelId={fData.data.single.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && fData ? <div>{sources}</div>
        : fError ? 
            <p>Please provid a valid ID</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalUpdatedSingle;
