import {useState} from 'react';

import MainBar from "../../../components/BuildingBlocks/MainBarTemplate";
import NewProd from "./CRUD/ADD/mainNewProduct";
import CategorySelection from "./CRUD/ADD/addCategory";
import SellerSelection from './CRUD/ADD/addSeller';
import TotalGHG from "./CRUD/ADD/addTotoalGHG";
import SingleGHG from "./CRUD/ADD/addSingleGHG";
import Reason from "./CRUD/ADD/addReason";
import Alternative from "./CRUD/ADD/addAlternative";
import RawMaterial from "./CRUD/ADD/addRawMaterial";

export type NewProductProps = {
    func: any,
    productId: string,
    setIdFunc: any,
    skipHandle: boolean,
};

const NewProduct = () => {

    const [number, setNumber] = useState(0);
    const [prodId, setProdId] = useState('');

    return (

        <>
            <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                    >
                        <div className="topBarFlex">
                            <div>
                                <h1 className="topBarHeadline">Add new products and services to Greenformed</h1>
                                <h3 className="topBarSubheadline">services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.services. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</h3>
                            </div>
                        </div>
                    </MainBar>
                    
                    <MainBar marginTop={'0'} marginBottom={'0'}>
                        <NewProd
                            func={() => setNumber(1)}
                            productId={""}
                            setIdFunc={(input: any) => setProdId(input)}
                            skipHandle={false}
                        ></NewProd>  
                    </MainBar>

                    {
                        number > 0 ?
                        <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <CategorySelection
                                func={() => setNumber(2)}
                                productId={prodId}
                                setIdFunc={''}
                                skipHandle={true}
                            ></CategorySelection>
                        </MainBar>
                        : <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <p>Before adding a category please add a product.</p>
                        </MainBar>
                    }

                    {
                        number > 1 ?
                        <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <SellerSelection
                                func={() => setNumber(3)}
                                productId={prodId}
                                setIdFunc={''}
                                skipHandle={true}
                            ></SellerSelection>
                        </MainBar>
                        : <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <p>Before adding a seller please add a product.</p>
                        </MainBar>
                    }

                    {
                        number > 2 ? 
                        <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <TotalGHG
                                func={() => setNumber(4)}
                                productId={prodId}
                                setIdFunc={''}
                                skipHandle={true}
                            ></TotalGHG>
                        </MainBar>
                        : <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <p>Before adding Green House Gas Data please fill the other fields.</p>
                        </MainBar>
                    }

                    {
                        number > 3 ?
                        <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <SingleGHG
                                func={() => setNumber(5)}
                                productId={prodId}
                                setIdFunc={''}
                                skipHandle={true}
                            ></SingleGHG>
                        </MainBar>
                        : <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <p>Before adding Green House Gas Data please fill the other fields.</p>
                        </MainBar>
                    }

                    {
                        number > 4 ? 
                        <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <Reason
                                func={() => setNumber(6)}
                                productId={prodId}
                                setIdFunc={''}
                                skipHandle={true}
                            ></Reason>
                        </MainBar> 
                        : <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <p>Before adding Green House Gas Data please fill the other fields.</p>
                        </MainBar>
                    } 

                    {
                        number > 5 ? 
                        <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <Alternative
                                func={() => setNumber(7)}
                                productId={prodId}
                                setIdFunc={''}
                                skipHandle={true}
                            ></Alternative>
                        </MainBar> 
                        : <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <p>Before adding Alternatives please fill the other fields.</p>
                        </MainBar>
                    }

                    {
                        number > 6 ? 
                        <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <RawMaterial
                                func={() => setNumber(8)}
                                productId={prodId}
                                setIdFunc={''}
                                skipHandle={true}
                            ></RawMaterial>
                        </MainBar> 
                        : <MainBar marginTop={'20px'} marginBottom={'0'}>
                            <p>Before adding the Raw Materials of the product please fill the other fields.</p>
                        </MainBar>
                    }
        </>
    )
};

export default NewProduct;
