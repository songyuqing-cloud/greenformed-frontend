import React, { useState } from 'react';
import MainBar from "../../../../BuildingBlocks/MainBarTemplate";
import { AuthContext } from "../../../../../App";
import UpdateForm from "../../../../BuildingBlocks/UpdateForm";
import UpdateFormNumber from "../../../../BuildingBlocks/UpdateFormNumber";
import RequestDeleteForm from "../../../../BuildingBlocks/requestDeleteForm";
import goTo from "../../../../../helper/goTo";

type ProductCardProps = {
    name: string, 
    description: string,
    linkToBuy: string,
    costPerProduct: number,
    imgLink: string,
    productId: string,
};

const UpdateMainData = (props: ProductCardProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [response, setResponse] = useState<string>('');
    const [description] = useState<string>(props.description);
    const [linkToBuy] = useState<string>(props.linkToBuy);
    const [imgLink] = useState<string>(props.imgLink);
    const [costs] = useState<number>(props.costPerProduct);
    const [descriptionChanged, setDescription] = useState<string>();
    const [linkToBuyChanged, setLinkToBuy] = useState<string>();
    const [imgLinkChanged, setImgLink] = useState<string>();
    const [costsCahnged, setCostsChanged] = useState<any>();
    const [selectedField, setSelectedField] = useState<string>('');
    const [sourceTitel, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDesc, setSourceDesc] = useState<string>('');

    const updateExistingData = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/product/update/fields/${props.productId}/${authState.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token    
                },
                body: JSON.stringify({
                    description: descriptionChanged,
                    imgLink: imgLinkChanged,
                    linkToBuy: linkToBuyChanged,
                    costPerProduct: costsCahnged,
                    sourceTitle: sourceTitel,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(res => {
                console.log(res)
                return res.json();
              })
              .then(() => {
                setResponse("success")
                setSourceTitle('')
                setSourceLink('')
                setSourceDesc('')
                setDescription('')
                setLinkToBuy('')
                setImgLink('')
                setCostsChanged(null)
                goTo('/product/' + props.productId)
              })
              .catch(error => {
                  console.log("error: "+error)
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

      const deleteData = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/product/request/deletion/${props.productId}/${authState.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token    
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitel,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(res => {
                console.log(res)
                return res.json();
              })
              .then(() => {
                setResponse("success")
                setSourceTitle('')
                setSourceLink('')
                setSourceDesc('')
                goTo('/product/' + props.productId)
              })
              .catch(error => {
                  console.log("error: "+error)
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="containerFormUpdateProduct">
                <div>
                    <h2>Name of the product: {props.name}</h2>
                    <p>Choose the field that you want to update: </p>

                    <div className='updateSelctButtonsDiv'>
                        <div className='updateSelectDivSelect' id='description' onClick={() => setSelectedField('description')}>Product Description</div>
                        <div className='updateSelectDivSelect' id='link' onClick={() => setSelectedField('link')}>Product Link To Buy</div>
                        <div className='updateSelectDivSelect' id='image' onClick={() => setSelectedField('image')}>Product Image Link</div>
                        <div className='updateSelectDivSelect' id='costs' onClick={() => setSelectedField('costs')}>Product Costs</div>
                        <div className='updateSelectDivSelect' id='delete' onClick={() => setSelectedField('delete')}>Request to delete the Product</div>
                    </div>

                    {
                        selectedField === 'description' ?
                        <form onSubmit={updateExistingData}>
                            <UpdateForm
                                eventUpdateField={setDescription}
                                label={'Update the product description:'}
                                typeUpdate={'text'}
                                value={descriptionChanged}
                                originalValue={description}
                                eventSourceTitle={setSourceTitle}
                                sourceTitle={sourceTitel}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                            <input type="submit" value="Submit" />
                        </form>
                        : selectedField === 'link' ?
                        <form onSubmit={updateExistingData}>
                            <UpdateForm
                                eventUpdateField={setLinkToBuy}
                                label={'Update the link to buy the product:'}
                                typeUpdate={'text'}
                                value={linkToBuyChanged}
                                originalValue={linkToBuy}
                                eventSourceTitle={setSourceTitle}
                                sourceTitle={sourceTitel}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                            <input type="submit" value="Submit" />
                        </form>
                        : selectedField === 'image' ?
                        <form onSubmit={updateExistingData}>
                            <UpdateForm
                                eventUpdateField={setImgLink}
                                label={'Update the image link of the product:'}
                                typeUpdate={"text"}
                                value={imgLinkChanged}
                                originalValue={imgLink}
                                eventSourceTitle={setSourceTitle}
                                sourceTitle={sourceTitel}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                            <input type="submit" value="Submit" />
                        </form>
                        : selectedField === 'costs' ?
                        <form onSubmit={updateExistingData}>
                            <UpdateFormNumber
                                eventUpdateField={setCostsChanged}
                                label={'Update the costs of the product:'}
                                typeUpdate={"number"}
                                value={costsCahnged}
                                originalValue={costs}
                                eventSourceTitle={setSourceTitle}
                                sourceTitle={sourceTitel}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                            <input type="submit" value="Submit" />
                        </form>
                        : selectedField === 'delete' ?
                        <form onSubmit={deleteData}>
                            <RequestDeleteForm
                                eventSourceTitle={setSourceTitle}
                                sourceTitle={sourceTitel}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                                name={props.name + ' (listed as a product in Greenformed) '}
                            />
                            <input type="submit" value="Submit" />
                        </form>
                        : <></>
                    }


                </div>
                {response ? <p>Success</p> : <></>}
            </div>
        </MainBar>
    )
};

export default UpdateMainData;