type SourcePointProps = {
    title: string, 
    status: string, 
    date: number, 
    link: string, 
    description: string, 
    userId: string,
    oriReasonTitle: string,
    oriDescription: string,
    oriGhgName: string,
};

const DeleteSource = (props: SourcePointProps) => {
    let date: any = new Date(props.date);
    let dateDate = date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getFullYear();
    let dateTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return (
        <div>
            <div className="dotSourceHis"></div> 
            <div>
                <p>Original Reason of GHG Emission: {props.oriReasonTitle}</p>
                <p>Original GHG emitted druing {props.oriReasonTitle}: {props.oriGhgName}</p>
                <p>Original Description of GHG Emission: {props.oriDescription}</p>
                <p>This source was added to prrof that the product needs to be deleted: </p>
                <p>Updated Source Title: {props.title}</p>
                <div>
                    <p>Source Link: </p>
                    <a href={props.link} target="_blank" rel="noreferrer">Click here</a>
                </div>
                <p>Source description: {props.description}</p>
                <p>Date of source creation: {dateDate} {dateTime}</p>
                <p>Approval status: {props.status}</p>
                {/* TODO: make link right */}
                <a href={`/user/${props.userId}`}>Visit User Profile</a>
            </div>  
        </div>
    )
};

export default DeleteSource;
