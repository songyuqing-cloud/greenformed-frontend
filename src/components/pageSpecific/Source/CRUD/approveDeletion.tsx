import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../BuildingBlocks/Skeleton";
import MainCard from '../../../BuildingBlocks/MainBarTemplate';
// import SourceDetails from "../sourceDetails";
// import ApprovalForm from "./approveSourceForm";
// import ApproveProduct from "./prodApproveSource";

const SourceApprovalDeletion = () => {
    const [pData, setPData] = useState<any>();
    const [sData, setSData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadTotalData();
        loadSourceData();
        // eslint-disable-next-line
      }, []);

    const loadTotalData = async () => {
        fetch('http://localhost:8000/api/o/product/get/full/' + id.product, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setPData(resData))
            .catch(err => setFError(err));
    }

    const loadSourceData = async () => {
        fetch('http://localhost:8000/api/o/source/get/id/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setSData(resData))
            .catch(err => setFError(err));
    } 

    //TODO: finish request to delete source

    const sources = pData && sData && !fError ? 
        <>
            {/* <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApprovalSource'>Approve or deny the deletion of the following source: {sData.data.source.title}</h1>
            </MainCard> 
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                   <h2 className='headlineApprovalSourceGHG'></h2>
            </MainCard> 
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
                <SourceDetails
                    name={pData.data.product.name}
                    titel={sData.data.source.title}
                    description={sData.data.source.description}
                    link={sData.data.source.link}
                    status={sData.data.source.status}
                ></SourceDetails>
            </MainCard>
            { sData.data.source.status !== 'approvedFinal' && sData.data.source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={pData.data.seller[0].sourceId}
                        modelId={pData.data.seller[0].id}
                        typeSource='seller'
                        productId={pData.data.product.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard> */}
        {/* } */}
        <p>{pData.data.product.name}</p>
        </>
        : <p>...Loading</p>

    return (
        <> { sources  && pData ? <div>{sources}</div>
        : fError ? 
            <p>Please provid a valid ID</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalDeletion;