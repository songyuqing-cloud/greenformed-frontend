import { useParams } from "react-router-dom";
import MainCard from "../../../BuildingBlocks/MainBarTemplate";
import SkeletonTemplate from "../../../BuildingBlocks/Skeleton";
import ProductCH from ".//CRUD/productGetChangeHistory";
import SellerChangeHistory from "./CRUD/sellerGetChangeHistory";
import ReasonChangeHistory from "./CRUD/reasonGetChangeHistory";
import SingleChangeHistory from "./CRUD/singleGetChangeHistory";

const ChangeHistoryController = () => {

    let id: any = useParams();

    return (
        <>
            {
                id.type === 'product' ?
                <ProductCH/>
                : 
                id.type === 'seller' ?
                <SellerChangeHistory/> 
                : 
                id.type === 'reason' ?
                <ReasonChangeHistory/> 
                : 
                id.type === 'single' ?
                <SingleChangeHistory/> 
                :
                <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    Please Provide correct data
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            }
        </>
    )
};

export default ChangeHistoryController;