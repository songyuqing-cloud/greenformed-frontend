import Search from "../BuildingBlocks/SearchBar";
import Button from "../BuildingBlocks/ButtonTemplate";

const SideBarTemplate = () => {
    return (
        <div className="sideBarTemplate">
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">GHG Data</h3>
                <p className="sideBarSubText">Callcuate the GHG Emission of Products and Services with our Calculator.</p>
                <Search 
                    event={console.log("test")} 
                    bg={"white"}
                    border={"#41475E"}
                    icon={"red"}
                    placeholder={"Search"}
                    fontColor={"#41475E"}
                    borderColor={"#41475E"}
                    width="130px"
                    changeEvent={() => console.log("test")}
                >
                Search
                </Search>
                <Button 
                    event={console.log("Main")}
                    bg={"#41475E"}
                    border={'#41475E'}
                    width={"215px"}
                    color={"white"}
                    margin={"10px 0 0 0"}
                >
                    Add Product
                </Button>
            </div>
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">Alterative Product</h3>
                <p className="sideBarSubText">Look up or add Prodcuct or Service that emmit less or even no GHG during production.</p>
                <Search 
                    event={console.log("test")} 
                    bg={"white"}
                    border={"#41475E"}
                    icon={"red"}
                    placeholder={"Search"}
                    fontColor={"#41475E"}
                    borderColor={"#41475E"}
                    width="130px"
                    changeEvent={() => console.log("test")}
                >
                Search
                </Search>
                <Button 
                    event={console.log("Main")}
                    bg={"#41475E"}
                    border={'#41475E'}
                    width={"215px"}
                    color={"white"}
                    margin={"10px 0 0 0"}
                >
                    Add Alterative
                </Button>
            </div>
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">Participate</h3>
                <p className="sideBarSubText">Add and review new products, add and update information or request to delete information.</p>
                <Button 
                    event={console.log("Main")}
                    bg={"#41475E"}
                    border={'#41475E'}
                    width={"215px"}
                    color={"white"}
                    margin={"10px 0 0 0"}
                >
                    Be Part of Greenformed
                </Button>
            </div>
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">For Businesses</h3>
                <p className="sideBarSubText">Contact Greenformed and learn about how businesses can benefit from our database and calculators.</p>
                <Button 
                    event={console.log("Main")}
                    bg={"#41475E"}
                    border={'#41475E'}
                    width={"215px"}
                    color={"white"}
                    margin={"10px 0 0 0"}
                >
                    More Information
                </Button>
            </div>
        </div>
    )
};

export default SideBarTemplate;