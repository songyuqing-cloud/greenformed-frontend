import EditSourceStatus from "../../BuildingBlocks/editSourceStatus";
import EditSimple from "../../BuildingBlocks/simpleEdit";

type ProductDetailsProps = {
    ghgName: string, 
    ghgEmission: string,
    GHGSourceEvent: any,
    GHGSoucreEventApprove: any,
    GHGEdit: any,
    GHGStatus: string,
    eventEdit: any,
    // index: number,
};

const ProductDetails = (props: ProductDetailsProps) => {

    return (
        <div className="ghgDetailContainer">
            <div className="ghgDetailPDiv">
                <p className="ghgDetailParagraph">{props.ghgName}</p>
                <p className="ghgDetailParagraphBrackets">({props.ghgEmission} KG)</p>
            </div>
            <EditSourceStatus 
                eventSource={props.GHGSourceEvent}
                eventEdit={props.GHGSoucreEventApprove}
                status={props.GHGStatus}
                // index={props.index}
            ></EditSourceStatus>
            <EditSimple
                event={props.eventEdit}
                editText={"Edit GHG Data"}
            ></EditSimple>

        </div>
    )
};

export default ProductDetails;
