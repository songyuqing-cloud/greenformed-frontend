import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import MainCard from "../../../../BuildingBlocks/MainBarTemplate";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import { AuthContext } from "../../../../../App";
import RequestDeleteForm from "../../../../BuildingBlocks/requestDeleteForm";
import goTo from "../../../../../helper/goTo";

const UpdateTotalData = () => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [sourceTitel, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDesc, setSourceDesc] = useState<string>('');
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/product/get/full/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

      const deleteData = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/ghg/total/request/deletion/${fData.data.totalEmission[0].id}/${authState.id}/${id.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token    
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitel,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(res => {
                console.log(res)
                return res.json();
              })
              .then(() => {
                setSourceTitle('')
                setSourceLink('')
                setSourceDesc('')
                goTo('/product/' + id.id)
              })
              .catch(error => {
                  console.log("error: "+error)
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <MainCard
            marginTop={"0"}
            marginBottom={'20px'}
        >
        { fData && !fError ?
            <div className="containerFormUpdateProduct">
                <div>
                    <form onSubmit={deleteData}>
                        <RequestDeleteForm
                            eventSourceTitle={setSourceTitle}
                            sourceTitle={sourceTitel}
                            eventSourceLink={setSourceLink}
                            sourceLink={sourceLink}
                            eventSourceDescription={setSourceDesc}
                            sourceDescr={sourceDesc}
                            name={fData.data.totalEmission[0].ghgPerKG + ' (listed as total GHG Emission of ' + fData.data.product.name + ' in Greenformed) '}
                        />
                        <input type="submit" value="Submit" />
                    </form>
                </div>
            </div>
            : fError ? 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <h1 className="errorMessageProductPage">Please enter a valid id to receive data from Greenformed!</h1>
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div> :
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        
        }
        </MainCard>
    )
};

export default UpdateTotalData;
