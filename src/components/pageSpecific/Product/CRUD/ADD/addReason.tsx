import React, {useState} from 'react';
import { AuthContext } from "../../../../../App";
import { NewProductProps } from "../../newProduct";
import Button from "../../../../BuildingBlocks/ButtonTemplate";

const ProductCard = (props: NewProductProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    let [timesSend, setTimesSend] = useState<number>(0);
    const [ghgName, setGHGName] = useState<string>('');
    const [reasonTitle, setReasonTitle] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [sourceTitle, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');

    let skipHandle: any = () => {
        props.func();
    }

    const postAddReasonGHG = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/ghg/reason/new/${authState.id}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitle,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    ghgName: ghgName,
                    reasonTitle: reasonTitle,
                    description: description,
                })
            }).then(res => {
                  return res.json();
              })
              .then((res) => {
                setSourceTitle("")
                setSourceLink("")
                setSourceDescription("")
                setTimesSend(1);
                setGHGName('')
                setReasonTitle('')
                setDescription('')
                props.func();
              })
              .catch(error => {
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <>
            {
                timesSend === 0 ? <h2 className="addProductCategoryHeadline">Add a reason of Green House Gases Emission: </h2> 
                : <h2 className="addProductCategoryHeadline">Add another reason for Green House Gases Emission: </h2>
            }
            <div className="addProductCategoryAddDiv">
                <form onSubmit={postAddReasonGHG}>
                    <h2 className="addProductCategoryHeadline">Add a new GHG Reason: </h2>
                    <label className="labelNewProduct">GHG Name *</label> 
                    <input className="form-control me-2"
                        id="inputNewSeller" 
                        type="text" 
                        placeholder="GHG Name"
                        aria-label="inputDescriptionProduct"
                        value={ghgName}
                        onChange={(e) => setGHGName(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Reason *</label> 
                    <input className="form-control me-2"
                        id="inputNewSeller" 
                        type="text" 
                        placeholder="Reason ABC"
                        aria-label="inputDescriptionProduct"
                        value={reasonTitle}
                        onChange={(e) => setReasonTitle(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Reason Description *</label> 
                    <textarea className="form-control me-2"
                        id="inputNewSellerSourceDescription" 
                        placeholder="Reason Description"
                        aria-label="inputDescriptionProduct"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        ></textarea>
                    <label className="labelNewProduct">Source Title *</label> 
                    <input className="form-control me-2"
                        id="inputNewSellerSourceLink" 
                        type="text" 
                        placeholder="Source Link"
                        aria-label="inputDescriptionProduct"
                        value={sourceTitle}
                        onChange={(e) => setSourceTitle(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Link *</label> 
                    <input className="form-control me-2"
                        id="inputNewSellerSourceLink" 
                        type="text" 
                        placeholder="Source Link"
                        aria-label="inputDescriptionProduct"
                        value={sourceLink}
                        onChange={(e) => setSourceLink(e.target.value)}
                        ></input>
                    <label className="labelNewProduct">Source Description *</label> 
                    <textarea className="form-control me-2"
                        id="inputNewSellerSourceDescription" 
                        placeholder="Source Description"
                        aria-label="inputDescriptionProduct"
                        value={sourceDescription}
                        onChange={(e) => setSourceDescription(e.target.value)}
                        ></textarea>
                    <input type="submit" value="Submit" />
                </form>
            </div>
            { props.skipHandle ?
                <Button
                    event={skipHandle}
                    bg={"#41475E"}
                    border={'transparaent'}
                    width={'250px'}
                    color={'white'}
                    margin={'10px 0 0 0'}
                >
                    Skip this field
                </Button> : <></>
            }
        </>
    )
};

export default ProductCard;