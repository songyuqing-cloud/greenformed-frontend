import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/Skeleton";
import MainCard from '../../../../BuildingBlocks/MainBarTemplate';
import SourceDetails from "../../sourceDetails";
import ApprovalForm from "../approveSourceForm";

const SourceApprovalUpdatedProduct = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('http://localhost:8000/api/o/seller/get/source/' + id.modelId + '/' + id.id, { 
            method: 'get', 
            headers: {},
          }).then(response => response.json())
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }

    const sources = fData ? 
        <>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApprovalSource'>Approve or deny the update source of the seller {fData.data.seller.name}</h1>
            </MainCard> 
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h2>The provided data of the updated seller: {fData.data.seller.name}</h2>
                <p>Link of seller: <a href={fData.data.seller.link} rel="noreferrer" target='_blank'>{fData.data.seller.link}</a></p>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
            <h2>The data was updated as following:</h2>
            <p>Changed Field: Link</p>
            <p>Original Value: {fData.data.updateSource[0].properties.OriginalValue}</p>
            <p>New Value: {fData.data.updateSource[0].properties.NewValue}</p>
                <SourceDetails
                    type={'updated seller'}
                    name={fData.data.seller.name}
                    titel={fData.data.updateSource[0].source.title}
                    description={fData.data.updateSource[0].source.description}
                    link={fData.data.updateSource[0].source.link}
                    status={fData.data.updateSource[0].source.status}
                ></SourceDetails>
            </MainCard>
            { fData.data.updateSource[0].source.status !== 'approvedFinal' && fData.data.updateSource[0].source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={fData.data.updateSource[0].source.id}
                        productId={id.product}
                        typeSource='seller'
                        modelId={fData.data.seller.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && fData ? <div>{sources}</div>
        : fError ? 
            <p>Please provid a valid ID</p>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalUpdatedProduct;
