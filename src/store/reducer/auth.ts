// TODO: check if type any is correct

const reducer = (state:any, action:any) => {
    switch (action.type) {
      case "LOGIN":
        localStorage.setItem("token", JSON.stringify(action.payload.data.token));
        localStorage.setItem("id", JSON.stringify(action.payload.data.id));
        localStorage.setItem("role", JSON.stringify(action.payload.data.role));
        localStorage.setItem("isAuthenticated", JSON.stringify(true));
        return {
          ...state,
          isAuthenticated: true,
          token: action.payload.data.token,
          id: action.payload.data.id,
          role: action.payload.data.role
        };
      case "LOGOUT":
        localStorage.clear();
        return {
          ...state,
          isAuthenticated: false
        };
      default:
        return state;
    }
  };

  export default reducer;