import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import FullSource from "../components/pageSpecific/Source/sourceHistoryController";

class Source extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed Source Page</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>
                <FullSource></FullSource>
            </Layout>
        )
    }
}

export default Source;