import { Button } from 'react-bootstrap';

type ButtonProps = {
    event: any;
    bg: string;
    children: string;
    border: string;
    width: string;
    color: string;
    margin: string;
};

const ButtonTemplate = (props: ButtonProps) => {
    return (
        <Button 
            onClick={props.event} 
            style={{background: props.bg, 
                borderColor: props.border,
                width: props.width,
                color: props.color,
                margin: props.margin
            }} 
            title="Button Component"
            >
            {props.children}
        </Button>
    )
};

export default ButtonTemplate;
