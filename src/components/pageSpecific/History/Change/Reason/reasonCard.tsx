type ProductCardProps = {
    ghgName: string, 
    description: string,
    reasonTitle: string,
};

const ProductCard = (props: ProductCardProps) => {
    return (
            <div>
                <h1>The change history of the reason of GHG Emission (current version): </h1>
                <p>Reason of GHG Emission: {props.reasonTitle}</p>
                <p>GHG Emitted during {props.reasonTitle}: {props.ghgName}</p>
                <p>Description: {props.description}</p>
            </div>
    )
};

export default ProductCard;
