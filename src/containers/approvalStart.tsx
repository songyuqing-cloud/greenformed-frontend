import  { Component } from 'react';
import { Helmet } from 'react-helmet';
import Layout from "../components/HOC/Layout";
import FetchApprovalData from '../components/pageSpecific/VerificationData/CRUD/fetchRandomVerificationData';

class Product extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed Product approve or deny product data</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                <FetchApprovalData></FetchApprovalData>
               
            </Layout>
        )
    }
}

export default Product;