import MainBar from "../../BuildingBlocks/MainBarTemplate";
import Tag from "../../BuildingBlocks/TagTemplate";
import SimpleEdit from "../../BuildingBlocks/simpleEdit";
import SourceAndEdit from "../../BuildingBlocks/editSourceStatus";
import Button from "../../BuildingBlocks/ButtonTemplate";
import goTo from "../../../helper/goTo";

type ProductMainInfoProps = {
    productId: string,
    productName: string, 
    productCode: string,
    seller: string,
    sellerId: string,
    totalGHG: number, 
    greenPremium: number, 
    category: string, 
    description: string, 
    imgUrl: string, 
    eventBuy: any,
    eventEditProduct: any,
    eventSource: any,
    eventEditSource: any,
    needsSourceEdit: boolean,
    status: string,
    sellerSourceHostoryEvent: any,
    sellerSourceApprove: any,
    sellerSourceStatus: string,
    eventEditSeller: any,
    eventAddSeller: any,
    sellerSourceId: string,
    productSourceId: string,

};

const ProductCard = (props: ProductMainInfoProps) => {
    const greenPremium: string = `Green Premium: ${props.greenPremium} %`;
    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="productMainInfoContainer">
                <div className="productColumn1">
                    <h2 className="productNameMainInfo">{props.productName}</h2>
                    <p className="productInfoTagsMainInfo">Category: {props.category}</p>
                    <div className="editInfoProductMainInfoDiv">
                    { 
                        props.category !== 'Not provided' ? 
                            <SimpleEdit
                                event={props.eventEditProduct}
                                editText="Edit Prodcut Data"
                            ></SimpleEdit>
                            : <SimpleEdit
                            event={() => goTo('/add/category/to/product/' + props.productId)}
                            editText="Add a category"
                        ></SimpleEdit>
                    }
                    </div>
                </div>
                <div className="productColumn2">
                    <p className="productCodeMainInfo">({props.productCode} | {props.seller})</p>
                    <SourceAndEdit
                            eventSource={() => goTo('/source/' + props.sellerSourceId + '/seller/' + props.seller)}
                            eventEdit={props.sellerSourceApprove}
                            status={props.sellerSourceStatus}
                        ></SourceAndEdit>
                        { 
                            props.seller !== 'Not provided' ? 
                            <>
                                <SimpleEdit
                                    event={props.eventEditSeller}
                                    editText="Edit Seller"
                                ></SimpleEdit>
                                <div onClick={() => goTo('/change/history/seller/' + props.sellerId)}>See Change History of the seller</div>
                            </> :
                            <SimpleEdit
                                event={props.eventAddSeller}
                                editText="Add Seller"
                            ></SimpleEdit>
                        }
                    <h3 className="productDescriptionMainInfo">{props.description}</h3>
                    { props.needsSourceEdit ? 
                    <div className="editInfoProductMainInfoDiv">
                        <SourceAndEdit
                            eventSource={() => goTo('/source/' + props.productSourceId + '/product/' + props.productName)}
                            eventEdit={props.eventEditSource}
                            status={props.status}
                        ></SourceAndEdit>
                    </div>
                    : <></>}
                </div>
                <div className="productColumn3">
                    {
                        props.totalGHG < 0 ? <Tag bg={"green"} width={'225px'} height={'35px'} color={"black"} fontSize={"16px"}>GHG Negative Product</Tag>
                        : props.totalGHG === 0 ? <Tag bg={"yellow"} width={'225px'} height={'35px'} color={"black"} fontSize={"16px"}>GHG Neutral Product</Tag>
                        : <Tag bg={"#F3C3C3"} width={'225px'} height={'35px'} color={"#571529"} fontSize={"16px"}>GHG High Product</Tag>
                    }
                    <Tag
                            bg={"#D8DCEE"} 
                            width={'224px'} 
                            height={'42px'} 
                            color={"#244B11"}
                            fontSize={"16px"}
                        >{greenPremium}
                    </Tag>
                    <Button 
                        event={props.eventBuy}
                        bg={"#41475E"}
                        width={"224px"}
                        color={"white"}
                        margin={"0"}
                        border={'none'}
                        >
                        Buy this product
                    </Button>
                </div>
                <div className="productColumn4">
                    <img src={props.imgUrl} alt={"Emissions of" + props.productName + ": " + props.totalGHG + " per kg."} className="imgProductMainInfo" />
                </div>
            </div>
            <div onClick={()=> goTo('/change/history/product/' + props.productId)}>Change History of {props.productName}</div>
        </MainBar>
    )
};

export default ProductCard;
