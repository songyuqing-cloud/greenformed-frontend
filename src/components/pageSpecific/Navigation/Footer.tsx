import React from 'react';
import Search from "../../BuildingBlocks/SearchBar";
import { Navbar, Nav } from 'react-bootstrap';
import logo from "../../../assests/LogoMock.png";
import user from "../../../assests/user.png";

// TODO: adapt to case

const Footer: React.FC = () => {

    return (
    <footer className="footerDiv">
        <Navbar bg="transparent" expand="lg" className="navbar">
            <Navbar.Brand href="/ome" className='footerBrand'>
                <div className="footerLogoDiv">
                    <img src={logo} alt="GreenFormed Logo" className="logoFooter" />
                </div>
                <h2 className="footerHeadline">Greenformed</h2>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse className="navbar-justify" id="basic-navbar-nav">
                <Search 
                    event={console.log("test")} 
                    bg={"#0B1B04"}
                    border={"#1E3D0E"}
                    icon={"red"}
                    placeholder={"Search"}
                    fontColor={"white"}
                    borderColor={"#1E3D0E"}
                    width="300px"
                    changeEvent={() => console.log("test")}
                >
                Search
                </Search>
                <Nav className="navbar-account">
                    <img src={user} alt="Greenformed User Account" className="accountImg" />
                    <Nav.Link href="/signup">Sign Up</Nav.Link>
                    <p className="deviderLine">|</p>
                    <Nav.Link eventKey={2} href="/login">
                        Log In
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
        
        <div className="footerBottom">
            <div>
                <div className="navBottom">
                    <div className="textDivNav">
                        <h2 className="textDivNavHeadline">Greenformed</h2>
                        <h3 className="textDivNavSubHead">The global GHG database</h3>
                        <br></br>
                        <p className="footerParagraph">Made by the community, for the community.</p>
                    </div>
                </div>
            </div>
            <div className="footerLeft">
                <div className="listOfPages">
                    <Nav.Link href="/" className="footerLink">
                        Start
                    </Nav.Link>
                    <Nav.Link href="/participate" className="footerLink">
                        Participate
                    </Nav.Link>
                    <Nav.Link href="/businesses" className="footerLink">
                        For Businesses
                    </Nav.Link>
                    <Nav.Link href="/api/documentation" className="footerLink">
                        API Documentation
                    </Nav.Link>
                    <Nav.Link href="/about" className="footerLink">
                        About Greenformed
                    </Nav.Link>
                </div>
                <div className="listOfPages">
                    <Nav.Link href="/imprint" className="footerLink">
                        Imprint
                    </Nav.Link>
                    <Nav.Link href="/legal/notice" className="footerLink">
                        Legal Notive
                    </Nav.Link>
                    <Nav.Link href="/conditions" className="footerLink">
                        Conditions 
                    </Nav.Link>
                </div>
                </div>
        </div>
        <div className="footerLeiste">
            <h4 className="footerLeisteText">Copyright 2021 greenformed.com | All rights reserved</h4>
        </div>
    </footer>
    );
};

export default Footer;

