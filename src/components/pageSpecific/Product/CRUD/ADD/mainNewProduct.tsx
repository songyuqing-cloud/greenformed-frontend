import React, {useState} from 'react';

import NewImage from "../../../../../assests/newImg.png";
import { NewProductProps } from "../../newProduct";
import { AuthContext } from "../../../../../App";

const MainNewProd = (props: NewProductProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [name, setName] = useState<string>('');
    const [linkToBuy, setLinkToBuy] = useState<string>('');
    const [costPerProduct, setCostPerProduct] = useState<number>(0);
    const [description, setDescription] = useState<string>('');
    const [imgLink] = useState<string>('https://www.test.de');
    const [sourceTitle] = useState<string>('Source Title des Source');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [isSet, setIsSet] = useState<boolean>(false);

    let eventNewImage:any = () => {
        console.log("Test")
    }

    const postNewProduct = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/product/new/${authState.id}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitle,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    name: name,
                    description: description,
                    linkToBuy: linkToBuy,
                    costPerProduct: costPerProduct,
                    imgLink: imgLink,
                })
            }).then(res => {
                return res.json();
              })
              .then((res) => {
                props.setIdFunc(res.data.id);
                props.func();
                setIsSet(true);
              })
              .catch(error => {
                setIsSet(false);
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.message
                });
              }
            );
      }

    return (
        <>
        {
            isSet === false ?
            <>
            <div className="newProductTopDiv">
                <div className="newProductMainInfosDiv">
                    <div className="newProductNameDiv">
                        <div className="newProductInputDiv">
                            <label>Product Name * </label> 
                            <input className="form-control me-2"
                                id="inputProductInfo" 
                                type="text" 
                                placeholder="Product Name"
                                aria-label="inputName"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                ></input>
                        </div>
                        <div className="newProductInputDiv">
                            <label>Link to buy</label> 
                            <input className="form-control me-2"
                                id="inputProductInfo" 
                                type="text" 
                                placeholder="Product Link"
                                aria-label="inputLinkToBuy"
                                value={linkToBuy}
                                onChange={(e) => setLinkToBuy(e.target.value)}
                                ></input>
                        </div>
                    </div>
                </div>
                <div className="newProductDescriptionDiv">
                    <label>Description *</label> 
                    <textarea className="form-control me-2"
                        id="inputProductInfoDescritption" 
                        placeholder="Product Description"
                        aria-label="inputDescriptionProduct"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        ></textarea>
                </div>
                <div className="newProductAddImageDiv" onClick={eventNewImage}>
                    <img src={NewImage} alt="Greenformed: add new data" className="newProductNewImg"/>
                </div>
            </div>
            <form onSubmit={postNewProduct}>
                <div className="sourceDivNewProduct">
                    <div className="sourceDivInputDiv">
                        <div className="newProductInputDivSource">
                                <label>Cost Per Product in EUR *</label> 
                                <input className="form-control me-2"
                                    id="inputProductInfoSourceLink" 
                                    type="number" 
                                    pattern='[0-9]{0,5}'
                                    aria-label="inputDescriptionProduct"
                                    value={costPerProduct}
                                    onChange={(e: any) => setCostPerProduct(+e.target.value)}
                                    ></input>
                        </div>
                        <div className="newProductInputDivSource">
                                <label>Source Link *</label> 
                                <input className="form-control me-2"
                                    id="inputProductInfoSourceLink" 
                                    type="text" 
                                    placeholder="Source Link"
                                    aria-label="inputDescriptionProduct"
                                    value={sourceLink}
                                    onChange={(e) => setSourceLink(e.target.value)}
                                    ></input>
                        </div>
                        <div className="newProductInputDivSource">
                                <label>Source Description *</label> 
                                <textarea className="form-control me-2"
                                    id="inputProductInfoSource" 
                                    placeholder="Description"
                                    aria-label="inputDescriptionProduct"
                                    value={sourceDescription}
                                    onChange={(e) => setSourceDescription(e.target.value)}
                                    ></textarea>
                        </div>
                    </div>
                    <div className="buttonDivNewData">
                        <input type="submit" value="Submit" />
                    </div>
                </div>
                {data.errorMessage && (
                        <span className="form-error" title="Error message">Please set all required fields and {data.errorMessage}</span>
                    )}
            </form>
            </> : 
            <div>
                <h3>{name} was successfully created!</h3>
                <p>Description: {description}</p>
                <p>{name} can be bought from: </p>
                <a href={linkToBuy} target="_blank" rel="noreferrer">{linkToBuy}</a>
                <p>{name} costs {costPerProduct} EUR.</p>
            </div>

        }
        </>
    )
};

export default MainNewProd;
