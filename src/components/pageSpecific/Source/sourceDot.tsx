type SourceDotProps = {
    approvalState: string,
    date: number, 
    description?: string,
    user: string, 
};

const SourceDot = (props: SourceDotProps) => {
    let date: any = new Date(props.date);
    let dateDate = date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getFullYear();
    let dateTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return (
        <div className='containerSourceDot'>
            <p className='dateSourceDot' >{dateDate + ", " + dateTime}</p> 
            <div className="dotSourceDots"></div>
            <div className="dotsTextDiv">
                <div className="dotsDataDiv">
                    <div className="dotsDataUpperDiv">
                        <p className='statusSourceDot'>{props.approvalState}</p>
                        <p className='userSourceDot'>{props.user}</p>

                    </div>
                    <p className='descriptionSourceDot'>{props.description}</p>
                </div>
            </div>
        </div>
    )
};

export default SourceDot;
