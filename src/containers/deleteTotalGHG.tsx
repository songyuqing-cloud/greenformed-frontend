import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import Total from "../components/pageSpecific/Product/CRUD/UPDATE/updateTotal";

class Product extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed: request to delete a GHG emission of a product and add new information to Greenformed</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                <Total></Total>
               
            </Layout>
        )
    }
}

export default Product; 