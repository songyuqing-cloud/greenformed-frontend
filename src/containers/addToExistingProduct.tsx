import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import AddToExisting from "../components/pageSpecific/Product/addDataToProduct";

class NewProduct extends Component {
    
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Add Data to an existing Product via Greenformed the Greenhouse Gas Platform.</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>
                
                <AddToExisting></AddToExisting>
                    
            </Layout>
        )
    }
}

export default NewProduct;