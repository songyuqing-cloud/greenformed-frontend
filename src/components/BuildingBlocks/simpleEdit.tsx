import edit from "../../assests/edit.png";

type SimpleEditProps = {
    event: any,
    editText: string,
};

const SimpleEdit = (props: SimpleEditProps) => {
    return (
        <div className='simpleEditContainer' onClick={props.event}>
            <img src={edit} alt="Greenformed edit Green House Gas Data for a product" className="simpleEditImg" />
            <p className="simpleEditParagraph">{props.editText}</p>
        </div>
    )
};

export default SimpleEdit;