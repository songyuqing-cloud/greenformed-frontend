type UpdateFormProps = {
    eventUpdateField: any;
    label: string,
    typeUpdate: string, 
    originalValue: any,
    value: any,
    eventSourceTitle: any,
    eventSourceLink: any,
    eventSourceDescription: any,
    sourceTitle: string,
    sourceLink: string,
    sourceDescr: string,
};

const UpdateForm = (props: UpdateFormProps) => {
    return (
        <>
            <p>The original values is: {props.originalValue}</p>
            <label className="labelUpdateProduct">{props.label}</label> 
            <input className="form-control me-2"
                id="inputUpdateLinkToBuy" 
                type={props.typeUpdate} 
                aria-label="inputUpdateLinkToBuy"
                value={props.value}
                onChange={(e) => props.eventUpdateField(e.target.value)}
                ></input>
            <label className="labelNewProduct">Add a source title</label> 
            <input className="form-control me-2"
                id="inputSourceUpdate" 
                type="text" 
                aria-label="inputSourceUpdate"
                value={props.sourceTitle}
                onChange={(e) => props.eventSourceTitle(e.target.value)}
                ></input>
            <label className="labelNewProduct">Add a source link</label> 
            <input className="form-control me-2"
                id="inputSourceLinkUpdate" 
                type="text" 
                aria-label="inputSourceLinkUpdate"
                value={props.sourceLink}
                onChange={(e) => props.eventSourceLink(e.target.value)}
                ></input>
            <label className="labelNewProduct">Add a source description</label> 
            <input className="form-control me-2"
                id="inputSourceDescUpdate" 
                type="text" 
                aria-label="inputSourceDescUpdate"
                value={props.sourceDescr}
                onChange={(e) => props.eventSourceDescription(e.target.value)}
                ></input>
        </>
    )
};

export default UpdateForm;