type SearchBarProps = {
    event: any;
    bg: string;
    fontColor: string;
    borderColor: string;
    border: string;
    children: string;
    icon: string;
    placeholder: string;
    width: string;
    changeEvent: any;
};

const SearchBarTemplate = (props: SearchBarProps) => {
    return (
        <>
            <input 
                className="form-control me-2"
                id="inputSearch" 
                type="search" 
                placeholder={props.placeholder} 
                aria-label="Search"
                style={{background: props.bg,
                        color: props.fontColor,
                        borderColor: props.borderColor, 
                        width: props.width  
                }} 
                onChange={props.changeEvent}
            />
            <button 
                className="btn btn-outline-success" 
                id="buttunSearch" 
                onClick={props.event}
                style={{background: props.bg, 
                    color: props.fontColor,
                    borderColor: props.borderColor }}
            >{props.children}
            </button>
        </>
    )
};

export default SearchBarTemplate;