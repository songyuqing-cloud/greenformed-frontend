import sourceImg from "../../assests/down-arrow-green.png";
import edit from "../../assests/edit.png";

type EditSourceStatusProps = {
    eventSource: any,
    eventEdit: any,
    status: string,
};

const EditSourceStatus = (props: EditSourceStatusProps) => {
    return (
        <div className="essContainer">
            <div className='essSourceDiv' onClick={props.eventSource}>
                <img src={sourceImg} alt="See the source of Green House Has Emissions at Greenformed" className="essImgSource" />
                <p className="simpleEditParagraph">Source</p>
            </div>
            <div className='essSourceDiv' onClick={props.eventEdit}>
                <img src={edit} alt="See the source of Green House Has Emissions at Greenformed" className="essImgSource" />
                <p className="simpleEditParagraph">Verify/Deny</p>
            </div>
            <div className="essSourceDiv">
                {
                    props.status === "newSource" ? <div className="statusPointNew" style={{background: 'red'}}></div>
                    : props.status === "approvedFinal" ? <div className="statusPointNew" style={{background: 'green'}}></div>
                    : <div className="statusPointNew" style={{background: 'orange'}}></div>
                }
                {
                    props.status === "approvedFinal" ? <p className="sourceStatusApproved">Source Approved</p>
                    : <p className="sourceStatusApproved">Not fully approved yet</p>
                }
            </div>
        </div>
    )
};

export default EditSourceStatus;