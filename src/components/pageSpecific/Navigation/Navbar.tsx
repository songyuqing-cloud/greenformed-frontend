import React from 'react';
import Search from "../../BuildingBlocks/SearchBar";
import { Navbar, Nav } from 'react-bootstrap';
import logo from "../../../assests/LogoMock.png";
import user from "../../../assests/user.png";
import { AuthContext } from "../../../App";
import goTo from "../../../helper/goTo";

// TODO: adapt to case

const Navigation: React.FC = () => {
    const { state: authState } = React.useContext(AuthContext);
    const { dispatch } = React.useContext(AuthContext);

    return (
    <header className="navContainer">
        <Navbar bg="transparent" expand="lg" className="navbar">
            <Navbar.Brand href="/ome">
                <div className="navLogoDiv">
                    <img src={logo} alt="GreenFormed Logo" className="logoNav" />
                </div>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse className="navbar-justify" id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link className="navLink" onClick={()=> goTo('/')}>Start</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/new/product')}>Add Data</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/approval/init/data')}>Approve Data</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/')}>For Businesses</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/')}>API Documentation</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/')}>About Greenformed</Nav.Link>
                </Nav>
                <div>
                    {!authState.token ? 
                        <Nav className="navbar-account">
                            <img src={user} alt="Greenformed User Account" className="accountImg" onClick={() => goTo('/user/profile')} />
                            <Nav.Link onClick={()=> goTo('/user/signup')}>Sign Up</Nav.Link>
                            <p className="deviderLine">|</p>
                            <Nav.Link eventKey={2} onClick={()=> goTo('/user/login')}>
                                Log In
                            </Nav.Link>
                        </Nav>
                        :   
                        <Nav className="navbar-account">
                            <img src={user} alt="Greenformed User Account" className="accountImg" onClick={() => goTo('/user/profile')} />
                            <Nav.Link onClick={() =>
                                    dispatch({
                                    type: "LOGOUT"
                                    })
                            }>Logout</Nav.Link>
                        </Nav>
                    }
                </div>
            </Navbar.Collapse>
        </Navbar>
        
        <div className="navBottom">
            <div className="textDivNav">
                <h2 className="textDivNavHeadline">Greenformed</h2>
                <h3 className="textDivNavSubHead">The global GHG database</h3>
            </div>
            <Search 
                event={console.log("test")} 
                bg={"#0B1B04"}
                border={"#1E3D0E"}
                icon={"red"}
                placeholder={"Search"}
                fontColor={"white"}
                borderColor={"#1E3D0E"}
                width="300px"
                changeEvent={() => console.log("test")}
            >
            Search
            </Search>
        </div>
    </header>
    );
};

export default Navigation;

