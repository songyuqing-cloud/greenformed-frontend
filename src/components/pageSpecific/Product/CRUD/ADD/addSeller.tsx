import React, {useState } from 'react';
import Button from "../../../../BuildingBlocks/ButtonTemplate";
import Search from "../../../../BuildingBlocks/SearchBar";
import SelectOptions from "../../../../BuildingBlocks/selectOptions";
import { AuthContext } from "../../../../../App";

import { NewProductProps } from "../../newProduct";

//todo debug; is set makes problems

const ProductCard = (props: NewProductProps) => {
    const { state: authState } = React.useContext(AuthContext);
    const initialState: object = {
        isSubmitting: false,
        errorMessage: null
    };
    const [data, setData] = useState<any>(initialState);
    const [seller, setSeller] = useState<string>('');
    const [existingSeller, setExistingSeller] = useState<any>();
    const [link, setLink] = useState<string>('');
    const [sourceTitle, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [isSet, setIsSet] = useState<boolean>(false);
    const [searchParam, setSearchParam] = useState<string>('');
    const [searchResult, setSearchResult] = useState<any>();

    let skipHandle: any = () => {
        props.func();
    }

    const searchForSeller = async () => {
        fetch(`http://localhost:8000/api/o/seller/get/${searchParam}`, { 
            method: 'GET', 
            headers: {
                "Content-Type": "application/json",   
            },
          }).then(response => response.json())
            .then(resData => setSearchResult(resData))
    }    

    const postAddNewSeller = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/seller/new/${authState.id}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token    
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitle,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    name: seller, 
                    link: link,
                })
            }).then(res => {
                  return res.json();
              })
              .then(() => {
                props.func();
                setIsSet(true);
              })
              .catch(error => {
                setIsSet(false);
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

      const putExistingSeller = (e: any) => {
        e.preventDefault();
        setData({
            ...data,
            isSubmitting: true,
            errorMessage: null
          });
    
        fetch(`http://localhost:8000/api/product/update/first/relationship/seller/${props.productId}/${existingSeller}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + authState.token
                },
            }).then(res => {
                  return res.json();
              })
              .then((res) => {
                props.func();
                setIsSet(true);
              })
              .catch(error => {
                setIsSet(false);
                setData({
                  ...data,
                  isSubmitting: false,
                  errorMessage: error.statusText
                });
              }
            );
      }

    return (
        <>
        {
            isSet === false ?
            <>
            <h2 className="addProductCategoryHeadline">Add the Company that sells this product</h2>
                <label>One seller is: </label>
                <div className="categorySelectDivFlex">
                            <Search
                                event={searchForSeller}
                                bg='white'
                                fontColor='grey'
                                borderColor='grey'
                                border='grey'
                                icon=''
                                placeholder='search existing categories'
                                width='300px'
                                changeEvent={(e: any) => setSearchParam(e.target.value)}
                            >Search</Search>
                            {
                                searchResult && searchResult.data.seller !== false ? 
                                <div>
                                    <SelectOptions
                                        event={() => setExistingSeller(searchResult.data.seller.dataValues.id)}
                                        text={searchResult.data.seller.dataValues.name}
                                    />
                                    { existingSeller ? 
                                        <Button
                                            event={putExistingSeller}
                                            bg={"#41475E"}
                                            border={'transparaent'}
                                            width={'250px'}
                                            color={'white'}
                                            margin={'10px 0 0 0'}
                                        >Set as seller</Button> : <></>
                                    }
                                    
                                </div> 
                                : searchResult && searchResult.data.seller === false ?  
                                <div>
                                    <p>{searchParam} does not exist in our database. Feel free to add it below.</p>
                                </div>
                                : <></>
                            }
                    <div className="addProductCategoryAddDiv">
                        <form onSubmit={postAddNewSeller}>
                            <h2 className="addProductCategoryHeadline">Or add a new seller: </h2>
                            <label className="labelNewProduct">Seller Name *</label> 
                            <input className="form-control me-2"
                                id="inputNewSeller" 
                                type="text" 
                                placeholder="Source Link"
                                aria-label="inputDescriptionProduct"
                                value={seller}
                                onChange={(e) => setSeller(e.target.value)}
                                ></input>
                            <label className="labelNewProduct">Seller Link *</label> 
                            <input className="form-control me-2"
                                id="inputNewSellerSourceLink" 
                                type="text" 
                                placeholder="Source Link"
                                aria-label="inputDescriptionProduct"
                                value={link}
                                onChange={(e) => setLink(e.target.value)}
                                ></input>
                            <label className="labelNewProduct">Source Title *</label> 
                            <input className="form-control me-2"
                                id="inputNewSellerSourceLink" 
                                type="text" 
                                placeholder="Source Title"
                                aria-label="inputDescriptionProduct"
                                value={sourceTitle}
                                onChange={(e) => setSourceTitle(e.target.value)}
                                ></input>
                            <label className="labelNewProduct">Source Description *</label> 
                            <textarea className="form-control me-2"
                                id="inputNewSellerSourceDescription" 
                                placeholder="Source Description"
                                aria-label="inputDescriptionProduct"
                                value={sourceDescription}
                                onChange={(e) => setSourceDescription(e.target.value)}
                                ></textarea>
                            <label className="labelNewProduct">Source Link *</label> 
                            <input className="form-control me-2"
                                id="inputNewSellerSourceLink" 
                                type="text" 
                                placeholder="Source Link"
                                aria-label="inputDescriptionProduct"
                                value={sourceLink}
                                onChange={(e) => setSourceLink(e.target.value)}
                                ></input>
                            <input type="submit" value="Submit" />
                        </form>
                    </div>
                </div>
                { props.skipHandle ?
                    <Button
                            event={skipHandle}
                            bg={"#41475E"}
                            border={'transparaent'}
                            width={'250px'}
                            color={'white'}
                            margin={'10px 0 0 0'}
                        >
                            Skip this field
                        </Button> : <></>
                }
        </> : seller && isSet ?
            <p>The seller is set to {seller}.</p>
        : <p>The seller is set.</p>
        }
       </>
    )
};

export default ProductCard;