type SellerCardProps = {
    name: string, 
    link: string,
};

const SellerCard = (props: SellerCardProps) => {
    return (
            <div>
                <h1>The change history of the following seller (current version): </h1>
                <p>Seller Name: {props.name}</p>
                <a href={props.link} target="_blank" rel="noreferrer">More Information</a>
            </div>
    )
};

export default SellerCard;
